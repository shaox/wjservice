//
//  Common.h
//  WangZhenVideo
//
//  Created by 邵 旭 on 14-10-14.
//  Copyright (c) 2014年 Tyrbl. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    defaultType = 0,                            //默认类型
    clickPark = 1,                              //点击园区
    clickSkill = 2,                             //点击技术
    clickTalent = 3,                            //点击人才
    clickCapital = 4,                           //点击资本
    clickClass = 5,                             //点击专题
    clickExperts = 6,                           //点击专家
    clickMechanism = 7,                         //点击机构
}HomeAction;                           //主页点击事件

/*****************************取得单类********************/
#define AFNetworkManage     [AFNetworkManager shareManagement]
#define AppManage           [AppContextManager shareInstanceManager]
#define SettingManage       [SettingManager shareManagement]
#define WjManage             [WujieApply shareInstanceManager]

/*****************************全局变量********************/
#define IsIOS7 ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)
#define IsIOS8 ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=8)
#define Verity_OnlyTel    @"^((13[0-9])|(15[^4,\\D])|(18[0,1,2,5-9]))\\d{8}$"       //电话号码的校验
#define Verity_EmailStyle @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"       //邮箱的格式
#define kWebLoadFaild @"http://mt.wujie.com.cn/index.php/user/nonework"
//定制输入框可输入限制
#define Verity_Password @"^[A-Za-z0-9]+$"                       //密码输入格式匹配校验
#define Verify_Username @"^[A-Za-z0-9._@]+$"                    //用户名输入匹配校验
#define Verity_OnlyNumber @"^[0-9]*$"                           //只能是数字的正则匹配内容,验证码匹配

/*****************************全局URL********************/
//mt：开发 test：测试
//#define Park            @"http://mt.wujie.com.cn/index.php/park/index/t/firstview"
//#define Technology      @"http://mt.wujie.com.cn/index.php/technology/index/t/firstview"
//#define Talent          @"http://mt.wujie.com.cn/index.php/person/index/t/firstview"
//#define Capital         @"http://mt.wujie.com.cn/index.php/capital/index/t/firstview"
//#define TrainClass      @"http://mt.wujie.com.cn/index.php/traincourse/index/t/firstview"
//#define TrainExperts    @"http://mt.wujie.com.cn/index.php/trainexpert/index/t/firstview"
//#define Mechanism       @"http://mt.wujie.com.cn/index.php/traininstitution/index/t/firstview"
//#define kSearchURL(param,paramStr) [NSString stringWithFormat:@"http://mt.wujie.com.cn/index.php/search/index/type/%@/keyword/%@",param,paramStr];
//#define kDemandURL      @"http://mt.wujie.com.cn/index.php?t=firstview&module=default&submodule=user&controller=postform&action=index"
//#define kServiceURL     @"http://mt.wujie.com.cn/index.php/servicenetwork/index/t/firstview"
//#define kSetingURL      @"http://mt.wujie.com.cn/index.php/set/index/t/firstview"
//#define kMeURL          @"http://mt.wujie.com.cn/index.php?t=firstview&module=default&submodule=user&controller=center&action=index"
//#define kVersonURL      @"http://mt.wujie.com.cn/index.php/versioninfo"
//#define kCityURL        @"http://mt.wujie.com.cn/index.php/citisinfo"

//#define Park            @"http://test.wujie.com.cn/index.php/park/index/t/firstview"
//#define Technology      @"http://test.wujie.com.cn/index.php/technology/index/t/firstview"
//#define Talent          @"http://test.wujie.com.cn/index.php/person/index/t/firstview"
//#define Capital         @"http://test.wujie.com.cn/index.php/capital/index/t/firstview"
//#define TrainClass      @"http://test.wujie.com.cn/index.php/traincourse/index/t/firstview"
//#define TrainExperts    @"http://test.wujie.com.cn/index.php/trainexpert/index/t/firstview"
//#define Mechanism       @"http://test.wujie.com.cn/index.php/traininstitution/index/t/firstview"
//#define kSearchURL(param,paramStr) [NSString stringWithFormat:@"http://test.wujie.com.cn/index.php/search/index/type/%@/keyword/%@",param,paramStr];
//#define kDemandURL      @"http://test.wujie.com.cn/index.php?t=firstview&module=default&submodule=user&controller=postform&action=index"
//#define kServiceURL     @"http://test.wujie.com.cn/index.php/servicenetwork/index/t/firstview"
//#define kSetingURL      @"http://test.wujie.com.cn/index.php/set/index/t/firstview"
//#define kMeURL          @"http://test.wujie.com.cn/index.php?t=firstview&module=default&submodule=user&controller=center&action=index"
//#define kVersonURL      @"http://test.wujie.com.cn/index.php/versioninfo"
//#define kCityURL        @"http://test.wujie.com.cn/index.php/citisinfo"

#define Park            @"http://app.wujie.com.cn/index.php/park/index/t/firstview"
#define Technology      @"http://app.wujie.com.cn/index.php/technology/index/t/firstview"
#define Talent          @"http://app.wujie.com.cn/index.php/person/index/t/firstview"
#define Capital         @"http://app.wujie.com.cn/index.php/capital/index/t/firstview"
#define TrainClass      @"http://app.wujie.com.cn/index.php/traincourse/index/t/firstview"
#define TrainExperts    @"http://app.wujie.com.cn/index.php/trainexpert/index/t/firstview"
#define Mechanism       @"http://app.wujie.com.cn/index.php/traininstitution/index/t/firstview"
#define kSearchURL(param,paramStr) [NSString stringWithFormat:@"http://app.wujie.com.cn/index.php/search/index/type/%@/keyword/%@",param,paramStr];
#define kDemandURL      @"http://app.wujie.com.cn/index.php?t=firstview&module=default&submodule=user&controller=postform&action=index"
#define kServiceURL     @"http://app.wujie.com.cn/index.php/servicenetwork/index/t/firstview"
#define kSetingURL      @"http://app.wujie.com.cn/index.php/set/index/t/firstview"
#define kMeURL          @"http://app.wujie.com.cn/index.php?t=firstview&module=default&submodule=user&controller=center&action=index"
#define kVersonURL      @"http://app.wujie.com.cn/index.php/versionInfo"
#define kCityURL        @"http://app.wujie.com.cn/index.php/citisinfo"
#define kNetworkNoti    @"www.baidu.com"

/*****************************全局color********************/
#define labelBgColor [UIColor colorWithRed:219/255.0f green:219/255.0f blue:218/255.0f alpha:1.0f]
#define backBgColor [UIColor colorWithRed:238/255.0f green:241/255.0f blue:245/255.0f alpha:1.0f]
#define textsColor [UIColor colorWithRed:235/255.0f green:115/255.0f blue:30/255.0f alpha:1.0f]
#define loginBarColor [UIColor colorWithRed:248/255.0f green:248/255.0f blue:248/255.0f alpha:1.0f]
//#define otherBarColor [UIColor colorWithRed:253/255.0f green:132/255.0f blue:94/255.0f alpha:1.0f]
#define otherBarColor [UIColor colorWithRed:236/255.0f green:115/255.0f blue:77/255.0f alpha:1.0f]
//首页
#define jnColor [UIColor colorWithRed:253/255.0f green:178/255.0f blue:51/255.0f alpha:1.0f]
#define rcColor [UIColor colorWithRed:144/255.0f green:176/255.0f blue:184/255.0f alpha:1.0f]
#define zbColor [UIColor colorWithRed:236/255.0f green:115/255.0f blue:77/255.0f alpha:1.0f]
#define ztColor [UIColor colorWithRed:234/255.0f green:185/255.0f blue:50/255.0f alpha:1.0f]
#define zjColor [UIColor colorWithRed:26/255.0f green:193/255.0f blue:237/255.0f alpha:1.0f]

///*****************************文件沙盒操作***************************/
//#define Document [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
//#define kMsgImagePath [Document stringByAppendingPathComponent:@"MsgPicture"]


/*****************************关于索引栏字母判断********************/
#define judgeletter_classic(ch) (((ch)>='A'&&ch<='Z')||((ch)>='a'&&(ch)<='z'))

/********************ios的3.5，4.0，4.7，5.5适配操作***************************/
#define kMainScreen_Height [UIScreen mainScreen].bounds.size.height
#define kMainScreen_Width [UIScreen mainScreen].bounds.size.width
#define kHomeCellHeightFor4 ([UIScreen mainScreen].bounds.size.height<568?458:[UIScreen mainScreen].bounds.size.height-110)

#define kNSLocalizedName        @"Main"             //国际化文件的名称

@interface Common : NSObject

@end
