//
//  CommonDeal.h
//  WangZhenVideo
//
//  Created by 邵 旭 on 14-10-14.
//  Copyright (c) 2014年 Tyrbl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ErrorModel.h"

@interface CommonDeal : NSObject
/**网络的统一的判断
 *\param        param:      无
 *\returns      return:     无
 */
+ (BOOL)checkNetwork;

/**无网络的统一调用
 *\param        param:      无
 *\returns      return:     无
 */
+ (void)showUnavalibleNetworkWarning;

/**UIAlertView的统一封装
 *\param        target:      委托对象
 *\param        titleOfPrompt:      标题
 *\param        promptMessage:      内容
 *\param        cancelBtnTitle:      取消按钮
 *\param        otherBtnTitleArray:      其它按钮的数组
 *\returns      return:     无
 */
+ (void)promptWithAlert:(id)target titleOfPrompt:(NSString *)promptTitle tag:(int)tags messageOfPrompt:(NSString *)promptMessage cancelButtonTitle:(NSString *)cancelBtnTitle otherButtonTitleArray:(NSArray *)otherBtnTitleArray;

/**去除字符串中的空格，换行等
 *\param        无
 *\param        无
 *\returns
 */
+ (NSString *)whitespaceAndNewlineCharacterSet:(NSString *)str;

/**解决NSUserDefualts中setobject的时候，偶尔无法保存的问题
 *\param        key:    key值
 *\param        value:    value的内容
 *\returns
 */
+(void)saveUserDefults:(NSString *)value andKey:(NSString *)key;

/**字典安全保护
 *\param        key:    key值
 *\param        newObjec:    value的内容
 *\param        dic:    字典
 *\returns
 */
+(void)setSafetyObject:(id)newObjec forKey:(id)key andDic:(NSMutableDictionary *)dic;

/**数组安全保护
 *\param        key:    key值
 *\param        array:    数组
 *\returns
 */
+(id)safetyArrayOjectAtIndex:(int)index andArray:(NSMutableArray *)array;

/**添加数组
 *\param        object:    要添加的对象
 *\param        array:    数组
 *\returns
 */
+(void)safetyAddObject:(id)object andArray:(NSMutableArray *)array;

/**数组中插入对象
 *\param        object:    要添加的对象
 *\param        index:    对象添加的位置
 *\param        array:    数组
 *\returns
 */
+(void)safetyInsertObject:(id)object atIndex:(int)index andArray:(NSMutableArray *)array;

/**判断当前的字符串是否为nil，[NULL null],@""
 *\param        str:    传入的字符串
 *\returns      BOOL:   yes表示是上面其中之一，no表示不是
 */
+(BOOL)jugementStringIsNil:(NSString *)str;

/**判断当前的数组是否为nil，[NULL null]
 *\param        arr:    传入的字符串
 *\returns      BOOL:   yes表示是上面其中之一，no表示不是
 */
+(BOOL)jugementArrIsNil:(NSArray *)arr;

/**判断当前是否是当前用户
 *\param        localUserID:        用户的userid
 *\returns      return:             是否是当前用户
 */
+ (BOOL)jugementIsLocalUser:(NSString *)userID;

/**图片上传过程中，将java的url路径中“/”替换为“_”样式，或者相反
 *\param        imageName:    发送消息是图片的java的url，本地存储是图片的名称
 *\param        flag:    yes表示本地存储，no表示消息发送
 *\returns      时间规则的字符串
 */
+(NSString *)JAVAImageNameToIOSImageName:(NSString *)imageName andFlag:(BOOL)flag;

/**时间戳转换
 *\param        intervalStr:    传入的字符串
 *\returns      format:         显示时间格式
 */
+ (NSString *)transferIntervalToStr:(NSString *)intervalStr andFormat:(NSString *)format;

/**获取当前时间
 *\param        param:      无
 *\returns      return:     MMddHHmmss的时间类型字符串
 */
+ (NSString *)getNowDateTime;
+(BOOL)checkDevice;

/**检查奔溃
 *\param        param:      无
 *\returns      return:     无
 */
void uncaughtExceptionHandler(NSException *exception);

/**获取网络图片
 *\param        param:      无
 *\returns      return:     无
 */
+(UIImage *) getImageFromURL:(NSString *)fileURL;

/**将数据写入磁盘
 *\param        param:      无
 *\returns      return:     无
 */
+(NSString *)writeData:(NSDictionary *)dic andNet:(BOOL)isNet;
@end
