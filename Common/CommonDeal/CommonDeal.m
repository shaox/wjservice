//
//  CommonDeal.m
//  WangZhenVideo
//
//  Created by 邵 旭 on 14-10-14.
//  Copyright (c) 2014年 Tyrbl. All rights reserved.
//

#import "CommonDeal.h"
#import "Reachability.h"
#import "AppContextManager.h"
#import "UserModel.h"

@implementation CommonDeal

+ (BOOL)checkNetwork
{
    Reachability *internetReach = [Reachability reachabilityForInternetConnection];
    if ([internetReach currentReachabilityStatus] == NotReachable)
    {
        return NO;
    }
    return YES;
}

+ (void)showUnavalibleNetworkWarning
{
    NSString *msg = @"连接失败";
    [[YBProgressShow defaultProgress] showText:msg
                                        InMode:MBProgressHUDModeText
                         OriginViewEnableTouch:NO
                            HideAfterDelayTime:1];
}

+ (void)promptWithAlert:(id)target titleOfPrompt:(NSString *)promptTitle tag:(int)tags messageOfPrompt:(NSString *)promptMessage cancelButtonTitle:(NSString *)cancelBtnTitle otherButtonTitleArray:(NSArray *)otherBtnTitleArray
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:promptTitle message:promptMessage delegate:target cancelButtonTitle:cancelBtnTitle otherButtonTitles:nil];
	alert.tag = tags;
	for (NSString *str in otherBtnTitleArray)
    {
		[alert addButtonWithTitle:str];
	}
	[alert show];
}

+ (NSString *)whitespaceAndNewlineCharacterSet:(NSString *)str
{
    str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
    str = [str stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}

+(void)saveUserDefults:(NSString *)value andKey:(NSString *)key
{
    NSUserDefaults *useD = [NSUserDefaults standardUserDefaults];
    [useD setObject:value forKey:key];
    [useD synchronize];
}

+ (BOOL)jugementIsLocalUser:(NSString *)userLRID
{
    UserModel *userM = AppManage.p_currentUserM;
    if([userLRID isEqualToString:userM.m_personID])
    {
        return YES;
    }
    return NO;
}

+(void)setSafetyObject:(id)newObjec forKey:(id)key andDic:(NSMutableDictionary *)dic
{
    #ifdef DEBUG//调测时候，出现崩溃newobjet为nil时候继续执行
        if(newObjec == nil)
        {
            NSLog(@"出现字典中设置nil");
        }
        [dic setObject:newObjec forKey:key];
    #else
        if(newObjec == nil)
        {
            
        }else{
            [dic setObject:newObjec forKey:key];
        }
    #endif
}

+(id)safetyArrayOjectAtIndex:(int)index andArray:(NSMutableArray *)array
{
        NSUInteger count = [array count];
    #ifdef DEBUG
        if(count ==0 || array == nil ||index>=count)
        {
            NSLog(@"数组越界，或者数组为nil");
        }
        return [array objectAtIndex:index];
    #else
        if(count ==0 || array == nil ||index>=count)
        {
            return  nil;
        }else{
            return [array objectAtIndex:index];
        }
    #endif
}

+(void)safetyAddObject:(id)object andArray:(NSMutableArray *)array
{
    #ifdef DEBUG
        if(object == nil)
        {
            NSLog(@"添加到数组中的对象为nil");
        }
        [array addObject:object];
    #else
        if(object != nil)
        {
            [array addObject:object];
        }
    #endif
}

+(void)safetyInsertObject:(id)object atIndex:(int)index andArray:(NSMutableArray *)array
{
    #ifdef DEBUG
        if(object == nil)
        {
            NSLog(@"添加到数组中的对象为nil");
        }
        [array insertObject:object atIndex:index];
    #else
        if(object != nil)
        {
            [array insertObject:object atIndex:index];
        }
    #endif
}

+(BOOL)jugementStringIsNil:(NSString *)str
{
    if(str == nil || [str class] == [NSNull null] || str == NULL)
    {
        return YES;
    }else
    {
        return NO;
    }
}

+(BOOL)jugementArrIsNil:(NSArray *)arr
{
    if(arr == nil || [arr class] == [NSNull null] || arr == NULL)
    {
        return YES;
    }else
    {
        return NO;
    }
}

+(NSString *)JAVAImageNameToIOSImageName:(NSString *)imageName andFlag:(BOOL)flag
{
    if(flag)//本地数据库存储
    {
        imageName = [imageName stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    }else{//消息交互
        imageName = [imageName stringByReplacingOccurrencesOfString:@"_" withString:@"/"];
    }
    return imageName;
}


//时间戳转化为时间
+ (NSString *)transferIntervalToStr:(NSString *)intervalStr andFormat:(NSString *)format
{
    NSDateFormatter *dateFrom=[[NSDateFormatter alloc] init];
	[dateFrom setDateFormat:format];
    NSString *currentDateStr = [dateFrom stringFromDate:[NSDate dateWithTimeIntervalSince1970:[intervalStr intValue]]];
    return currentDateStr;
}

//获取当前时间
+ (NSString *)getNowDateTime
{
    NSDate *nowDate = [NSDate date];
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"MMddHHmmss"];
    NSString *str = [dateformat stringFromDate:nowDate];
    return str;
}

+ (NSRange)stringRangeContainsEmoji:(NSString *)string
{
    __block NSRange range = NSMakeRange(0, 0);
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop)
     {
         const unichar hs = [substring characterAtIndex:0];
         // surrogate pair
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     if(range.length == 0&&range.location == 0)
                     {
                         range = enclosingRange;
                     }
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 if(range.length == 0&&range.location == 0)
                 {
                     range = enclosingRange;
                 }
             }
             
         } else {
             if (0x2100 <= hs && hs <= 0x27ff) {
                 if(range.length == 0&&range.location == 0)
                 {
                     range = enclosingRange;
                 }
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 if(range.length == 0&&range.location == 0)
                 {
                     range = enclosingRange;
                 }
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 if(range.length == 0&&range.location == 0)
                 {
                     range = enclosingRange;
                 }
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 if(range.length == 0&&range.location == 0)
                 {
                     range = enclosingRange;
                 }
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                 if(range.length == 0&&range.location == 0)
                 {
                     range = enclosingRange;
                 }
             }
         }
     }];
    return range;
}


+ (BOOL)stringContainsEmoji:(NSString *)string
{
    __block BOOL returnValue = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop)
     {
         const unichar hs = [substring characterAtIndex:0];
         // surrogate pair
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     returnValue = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 returnValue = YES;
             }
             
         } else {
             if (0x2100 <= hs && hs <= 0x27ff) {
                 returnValue = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 returnValue = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 returnValue = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 returnValue = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                 returnValue = YES;
             }
         }
     }];
    return returnValue;
}


+(BOOL)checkDevice
{
    NSString* deviceType = [UIDevice currentDevice].model;
    NSLog(@"deviceType = %@", deviceType);
    
    NSRange range = [deviceType rangeOfString:@"iPhone"];
    return range.location != NSNotFound;
}

//崩溃出现的情况下，保存崩溃信息到本地
void uncaughtExceptionHandler(NSException *exception)
{
    NSArray *stackArray = [exception callStackSymbols];//异常的堆栈信息
    NSString *reason = [exception reason];//异常的原因
    NSString *name = [exception name];//异常的名称
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateStr = [dateFormatter stringFromDate:[NSDate date]];//异常出现的时间
    NSString *exceptionInfo = [NSString stringWithFormat:@"Exception create Time：%@\nException reason：%@\nException name：%@\nException stack：%@",dateStr,reason, name, stackArray];
    NSLog(@"exceptionInfo:%@",exceptionInfo);
}

+(UIImage *) getImageFromURL:(NSString *)fileURL {
    NSLog(@"执行图片下载函数");
    UIImage * result;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    return result;
    
}

+(NSString *)writeData:(NSDictionary *)dic andNet:(BOOL)isNet
{
    //1. 创建一个plist文件
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *filename = nil;
    if (isNet) {
        filename = [path stringByAppendingPathComponent:@"serviceCity.plist"];
    }else{
        filename = [path stringByAppendingPathComponent:@"allCity.plist"];
    }
    NSFileManager* fm = [NSFileManager defaultManager];
    [fm createFileAtPath:filename contents:nil attributes:nil];
    [dic writeToFile:filename atomically:YES];
    return filename;
}

@end
