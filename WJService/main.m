//
//  main.m
//  WJService
//
//  Created by 邵 旭 on 15-1-30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
