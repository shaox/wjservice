//
//  AppDelegate.m
//  WJService
//
//  Created by 邵 旭 on 15-1-30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "AppDelegate.h"
#import "FirstViewController.h"
#import "TabbarViewController.h"
#import "Reachability.h"
#import "AppContextManager.h"
#import "WujieApply.h"
@interface AppDelegate ()
{
    Reachability *m_reachability;
}
@property(nonatomic,strong) Reachability *p_reachability;
@end

@implementation AppDelegate
@synthesize p_reachability = m_reachability;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self startNotificationNetwork];    //开启网络状况的监听
    [self jugmentIsFirstExe];           //判断是否是第一次登录，设置root
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);//记录崩溃的信息
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)startNotificationNetwork
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:@"kReachabilityChangedNotification" object:nil];
    self.p_reachability = [Reachability reachabilityWithHostName:kNetworkNoti];
    [m_reachability startNotifier];
}

//网络链接改变时会调用的方法
-(void)reachabilityChanged:(NSNotification *)note
{
    Reachability *currReach = [note object];
    NSParameterAssert([currReach isKindOfClass:[Reachability class]]);

    //对连接改变做出响应处理动作
    NetworkStatus status = [currReach currentReachabilityStatus];
    //如果没有连接到网络就弹出提醒实况
    if(status == NotReachable){
        AppManage.p_noNetWork = YES;
    }
    else{
        [self sendVerson];
        AppManage.p_noNetWork = NO;
    }
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"Network" object:nil];
}

//判断是否首次运行应用程序
- (void)jugmentIsFirstExe
{
    // 本地存储完成后可开启自动登录，将queryLogin方法移除，此时app可以离线登录
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NSUserDefaults *userD = [NSUserDefaults standardUserDefaults];
    BOOL firstExe = [userD boolForKey:@"FirstExe"];
    if(firstExe)//如果存在非首次运行
    {
        TabbarViewController *tabbar = [storyboard instantiateViewControllerWithIdentifier:@"HomeID"];
        [self.window setRootViewController:tabbar];
    }else{//首次运行，进入首页引导
        [userD setBool:YES forKey:@"FirstExe"];
        [userD synchronize];
        FirstViewController *first = [storyboard instantiateViewControllerWithIdentifier:@"FirstID"];
        [self.window setRootViewController:first];
    }
}

-(void)sendVerson
{
    if (!AppManage.p_allPCArray) {
        NSString *verson = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
        [WjManage sendVerson:verson andSuccessBlock:^(id backStr) {
            [self getProvince:NO];
        } andFailedBlock:^(ErrorModel *errorM) {
            
        }];
    }
}

-(void)getProvince:(BOOL)isNet
{
    [WjManage getCity:isNet andSuccessBlock:^(id backStr) {
        NSMutableArray *arr = (NSMutableArray *)backStr;
        NSArray *sortedArray = [arr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSInteger val1 = ((ProvinceModel *)obj1).m_provinceID.integerValue;
            NSInteger val2 = ((ProvinceModel *)obj2).m_provinceID.integerValue;
            //升序，假如需要降序的话，只需要修改下面的逻辑
            if (val1 < val2){
                return NSOrderedAscending;
            }else{
                return NSOrderedDescending;
            }
        }];
        if (isNet) {
            AppManage.p_servicePCArray = sortedArray;
            [[NSNotificationCenter defaultCenter]postNotificationName:@"Service" object:nil];
        }
        else{
            AppManage.p_allPCArray = sortedArray;
            [self getProvince:YES];
        }
    } andFailedBlock:^(ErrorModel *errorM) {
        [[YBProgressShow defaultProgress] showText:errorM.m_message
                                            InMode:MBProgressHUDModeText
                             OriginViewEnableTouch:NO
                                HideAfterDelayTime:1];
    }];
}

@end
