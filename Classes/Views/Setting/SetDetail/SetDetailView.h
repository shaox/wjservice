//
//  SetDetailView.h
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SetDetailDelegate <NSObject>

-(void)webviewTitleFormSet:(NSString *)titleStr;
-(void)gotoFirst:(NSString *)url;

@end

@interface SetDetailView : UIView<UIWebViewDelegate>
{
    UIWebView *m_setWebView;
    __weak id<SetDetailDelegate> m_setDetailDelegate;
}
@property (nonatomic,strong) IBOutlet UIWebView *p_setWebView;
@property (nonatomic,weak) id<SetDetailDelegate> p_setDetailDelegate;

-(void)loadServiceSetDetail:(NSString *)URL;

@end
