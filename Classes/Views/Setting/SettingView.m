//
//  SettingView.m
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "SettingView.h"
#import "LoadFaild.h"
#import "AppContextManager.h"
@interface SettingView ()<LoadFaildDelegate>
{
    LoadFaild *m_loadFaild;
    NSString *m_loadString;
}
@property(nonatomic,strong)LoadFaild *p_loadFaild;
@property(nonatomic,strong)NSString *p_loadString;
@end

@implementation SettingView
@synthesize p_loadFaild = m_loadFaild;
@synthesize p_loadString = m_loadString;
@synthesize p_settingWebView = m_settingWebView;
@synthesize p_settingDelegate = m_settingDelegate;

-(void)dealloc
{
    
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if (self) {
        self.p_loadFaild = [[LoadFaild alloc]initWithFrame:CGRectMake(0, 0, kMainScreen_Width, kMainScreen_Height-113)];
        m_loadFaild.p_loadFaildDelegate = self;
        m_loadFaild.hidden = YES;
        [self addSubview:m_loadFaild];
    }
    return self;
}

-(void)loadSettingWeb:(NSString *)URL
{
    //从网络加载
    [m_settingWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
}

#pragma mark - LoadFaildDelegate
-(void)reloadTheWebView:(id)sender
{
    [m_settingWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:m_loadString]]];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // starting the load, show the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    m_loadFaild.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // finished loading, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // load error, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    m_loadFaild.hidden = NO;
}

//在这个回调方法中，我们会得到服务器传过来的数据request，我们可以判断穿过来的参数，然后进行判断调用oc的方法
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"request:%@",request.URL.absoluteString);
    NSRange strRange = [request.URL.absoluteString rangeOfString:@"firstview"];
    NSRange strRange2 = [request.URL.absoluteString rangeOfString:@"/logout"];
    self.p_loadString = request.URL.absoluteString;
    NSRange blank = [request.URL.absoluteString rangeOfString:@"about:blank"];
    NSRange network = [request.URL.absoluteString rangeOfString:@"isNetworkConnected"];
    NSRange app = [request.URL.absoluteString rangeOfString:@"toScore"];
    if (blank.location != NSNotFound){
        return NO;
    }
    else if (network.location != NSNotFound){
        if (AppManage.p_noNetWork) {
            [m_settingWebView stringByEvaluatingJavaScriptFromString:@"sendNetState(false)"];
            [[YBProgressShow defaultProgress] showText:@"无网络"
                                                InMode:MBProgressHUDModeText
                                 OriginViewEnableTouch:NO
                                    HideAfterDelayTime:1.5f];
        }
        return NO;
    }
    else if (app.location != NSNotFound){
        NSString *str = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=981501194";
        BOOL bOpen =  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        NSLog(@"%d",bOpen);
        return NO;
    }
    else if (strRange.location == NSNotFound && strRange2.location == NSNotFound){
        if (m_settingDelegate && [m_settingDelegate respondsToSelector:@selector(showSetDetail:)]) {
            [m_settingDelegate showSetDetail:request.URL.absoluteString];
        }
        return NO;
    }
    else if (strRange2.location != NSNotFound){
        if (m_settingDelegate && [m_settingDelegate respondsToSelector:@selector(logoutApp:)]) {
            [m_settingDelegate logoutApp:request.URL.absoluteString];
        }
        return YES;
    }
    return YES;
}


@end
