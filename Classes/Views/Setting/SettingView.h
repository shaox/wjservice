//
//  SettingView.h
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingDelegate <NSObject>

-(void)showSetDetail:(NSString *)url;
-(void)logoutApp:(NSString *)url;
@end
@interface SettingView : UIView<UIWebViewDelegate>
{
    UIWebView *m_settingWebView;
    __weak id<SettingDelegate> m_settingDelegate;
}
@property (nonatomic,strong) IBOutlet UIWebView *p_settingWebView;
@property (nonatomic,weak) id<SettingDelegate> p_settingDelegate;

-(void)loadSettingWeb:(NSString *)URL;

@end
