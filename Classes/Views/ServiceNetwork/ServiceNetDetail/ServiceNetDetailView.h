//
//  ServiceNetDetailView.h
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NetworkDetailDelegate <NSObject>

-(void)webviewTitleFormNetwork:(NSString *)titleStr;
-(void)gotoFirstNet:(NSString *)url;
@end

@interface ServiceNetDetailView : UIView<UIWebViewDelegate>
{
    UIWebView *m_netWebView;
    __weak id<NetworkDetailDelegate> m_networkDetailDelegate;
}
@property (nonatomic,strong) IBOutlet UIWebView *p_netWebView;
@property (nonatomic,weak) id<NetworkDetailDelegate> p_networkDetailDelegate;

-(void)loadServiceNetworkDetail:(NSString *)URL;
@end
