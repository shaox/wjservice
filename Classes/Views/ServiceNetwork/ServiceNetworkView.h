//
//  ServiceNetworkView.h
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ServiceNetworkDelegate <NSObject>

-(void)showNetworkDetail:(NSString *)networkUrl;
-(void)selectNetAddress:(NSString *)selectStr;
-(void)setHeadActionSheet:(NSString *)number;
-(void)networkSelect:(id)sender;

@end

@interface ServiceNetworkView : UIView<UIWebViewDelegate>
{
    UIWebView *m_serviceNetworkWebView;
    __weak id<ServiceNetworkDelegate> m_serviceNetworkDelegate;
}
@property (nonatomic,strong) IBOutlet UIWebView *p_serviceNetworkWebView;
@property (nonatomic,weak) id<ServiceNetworkDelegate> p_serviceNetworkDelegate;

-(void)loadServiceNetworkWeb:(NSString *)URL;
-(void)sendNetResultString:(NSString *)resultStr;

@end
