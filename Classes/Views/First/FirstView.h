//
//  FirstView.h
//  WJService
//
//  Created by 邵 旭 on 15/3/18.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FirstDelegate <NSObject>

/*!选择进入界面
 *\prama            无
 *\returns          无
 */
-(void)gotoLogin:(id)sender;

@end
@interface FirstView : UIView
{
    __weak id<FirstDelegate> m_firstDelegate;
}
@property(nonatomic,weak) id<FirstDelegate> p_firstDelegate;


/*!视图圆角
 *\prama            无
 *\returns          无
 */
-(void)refreshFirstView;

-(void)scrollView;
@end
