//
//  FirstView.m
//  WJService
//
//  Created by 邵 旭 on 15/3/18.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "FirstView.h"

@interface FirstView ()<UIScrollViewDelegate>
{
    UIScrollView *m_scrollView;
    UIPageControl *m_pageControl;
}
@property (nonatomic,strong) IBOutlet UIScrollView *p_scrollView;;
@property (nonatomic,strong) IBOutlet UIPageControl *p_pageControl;

@end

@implementation FirstView
@synthesize p_scrollView = m_scrollView;
@synthesize p_pageControl = m_pageControl;
@synthesize p_firstDelegate = m_firstDelegate;

-(void)dealloc
{
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self refreshFirstView];
    }
    return self;
}

-(void)refreshFirstView
{
    m_pageControl.numberOfPages = 3;    
    for (int i = 0; i< m_pageControl.numberOfPages; i++)
    {
        UIImageView *imaContent = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"First-%d.jpg",i]]];
        imaContent.frame = CGRectMake(i*kMainScreen_Width, 0, kMainScreen_Width, kMainScreen_Height);
        [m_scrollView addSubview:imaContent];
        if (i == m_pageControl.numberOfPages-1) {
            UIButton *gotoLoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            gotoLoginBtn.frame = CGRectMake(i*kMainScreen_Width+kMainScreen_Width/4+7, kMainScreen_Height-kMainScreen_Height/5, (kMainScreen_Width/2)-14, kMainScreen_Height/15);
            gotoLoginBtn.layer.masksToBounds = YES;
            gotoLoginBtn.layer.cornerRadius = 5;
            gotoLoginBtn.layer.borderColor = [[UIColor whiteColor]CGColor];
            gotoLoginBtn.layer.borderWidth = 1.5f;
            gotoLoginBtn.titleLabel.font = [UIFont systemFontOfSize:18];
            [gotoLoginBtn setTitle:@"立即体验" forState:UIControlStateNormal];
            [gotoLoginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [gotoLoginBtn setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.5f] forState:UIControlStateHighlighted];
            [gotoLoginBtn addTarget:self action:@selector(gotoLoginView:) forControlEvents:UIControlEventTouchUpInside];
            [m_scrollView addSubview:gotoLoginBtn];
        }
    }
}

-(void)scrollView
{
    if (IsIOS8) {
        m_scrollView.contentSize = CGSizeMake(kMainScreen_Width*m_pageControl.numberOfPages, kMainScreen_Height);
    }
    else{
        m_scrollView.contentSize = CGSizeMake(self.frame.size.width*m_pageControl.numberOfPages, kMainScreen_Height-20);
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = m_scrollView.contentOffset.x/kMainScreen_Width;
    m_pageControl.currentPage = page;
}

#pragma mark - PageControl

- (IBAction)changePage:(id)sender {
    
    NSInteger page = m_pageControl.currentPage;                 //获取当前pagecontroll的值
    [m_scrollView setContentOffset:CGPointMake(kMainScreen_Width * page, 0)];   //根据pagecontroll的值来改变scrollview的滚动位置，以此切换到指定的页面
}

-(void)gotoLoginView:(id)sender
{
    if (m_firstDelegate && [m_firstDelegate respondsToSelector:@selector(gotoLogin:)]) {
        [m_firstDelegate gotoLogin:nil];
    }
}

@end
