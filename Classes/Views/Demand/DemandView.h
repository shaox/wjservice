//
//  DemandView.h
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DemandDelegate <NSObject>

-(void)showDemandBar:(NSString *)detailUrl;
-(void)showDemandTab:(BOOL)isShow;

@end
@interface DemandView : UIView<UIWebViewDelegate>
{
    UIWebView *m_demandWebView;
    __weak id<DemandDelegate> m_demandDelegate;
}
@property (nonatomic,strong) IBOutlet UIWebView *p_demandWebView;
@property (nonatomic,weak) id<DemandDelegate> p_demandDelegate;

-(void)loadServiceDemand:(NSString *)URL;

@end
