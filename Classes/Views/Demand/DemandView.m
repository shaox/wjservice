//
//  DemandView.m
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "DemandView.h"
#import "LoadFaild.h"
#import "AppContextManager.h"
@interface DemandView ()<LoadFaildDelegate>
{
    LoadFaild *m_loadFaild;
    NSString *m_loadString;
}
@property(nonatomic,strong)LoadFaild *p_loadFaild;
@property(nonatomic,strong)NSString *p_loadString;
@end

@implementation DemandView
@synthesize p_loadFaild = m_loadFaild;
@synthesize p_loadString = m_loadString;
@synthesize p_demandWebView = m_demandWebView;
@synthesize p_demandDelegate = m_demandDelegate;

-(void)dealloc
{
    
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if (self) {
        self.p_loadFaild = [[LoadFaild alloc]initWithFrame:CGRectMake(0, 0, kMainScreen_Width, kMainScreen_Height-113)];
        m_loadFaild.p_loadFaildDelegate = self;
        m_loadFaild.hidden = YES;
        [self addSubview:m_loadFaild];
    }
    return self;
}

-(void)loadServiceDemand:(NSString *)URL
{
    //从网络加载
    [m_demandWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
}

#pragma mark - LoadFaildDelegate
-(void)reloadTheWebView:(id)sender
{
    [m_demandWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:m_loadString]]];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // starting the load, show the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    m_loadFaild.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // finished loading, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // load error, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    m_loadFaild.hidden = NO;
}

//在这个回调方法中，我们会得到服务器传过来的数据request，我们可以判断穿过来的参数，然后进行判断调用oc的方法
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"request:%@",request.URL.absoluteString);
    self.p_loadString = request.URL.absoluteString;
    NSRange strRange = [request.URL.absoluteString rangeOfString:@"firstview"];
    NSRange blank = [request.URL.absoluteString rangeOfString:@"about:blank"];
    NSRange network = [request.URL.absoluteString rangeOfString:@"isNetworkConnected"];
    if (blank.location != NSNotFound){
        return NO;
    }
    else if (network.location != NSNotFound){
        if (AppManage.p_noNetWork) {
            [m_demandWebView stringByEvaluatingJavaScriptFromString:@"sendNetState(false)"];
            [[YBProgressShow defaultProgress] showText:@"无网络"
                                                InMode:MBProgressHUDModeText
                                 OriginViewEnableTouch:NO
                                    HideAfterDelayTime:1.5f];
        }
        return NO;
    }
    else if (strRange.location == NSNotFound){
        if (m_demandDelegate && [m_demandDelegate respondsToSelector:@selector(showDemandBar:)]) {
            [m_demandDelegate showDemandBar:request.URL.absoluteString];
        }
        return NO;
    }
    return YES;
}

@end
