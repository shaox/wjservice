//
//  DemandDetailView.h
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DemandDetailDelegate <NSObject>

-(void)webviewTitleFormDemand:(NSString *)titleStr;
-(void)showDemandFirst:(NSString *)url andMethod:(NSInteger)index;
-(void)selectDemandAddress:(NSString *)cityStr;
-(void)demandDetailSelect:(id)sender;
@end

@interface DemandDetailView : UIView<UIWebViewDelegate>
{
    UIWebView *m_demandWebView;
    __weak id<DemandDetailDelegate> m_demandDetailDelegate;
}
@property (nonatomic,strong) IBOutlet UIWebView *p_demandWebView;
@property (nonatomic,weak) id<DemandDetailDelegate> p_demandDetailDelegate;

-(void)loadServiceDemandDetail:(NSString *)URL;
-(void)sendDemandResultString:(NSString *)resultStr;
@end
