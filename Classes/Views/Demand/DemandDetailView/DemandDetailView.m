//
//  DemandDetailView.m
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "DemandDetailView.h"
#import "ProvinceModel.h"
#import "CityModel.h"
#import "LoadFaild.h"
#import "AppContextManager.h"
@interface DemandDetailView ()<LoadFaildDelegate>
{
    NSMutableArray *m_dataArray;
    LoadFaild *m_loadFaild;
    NSString *m_loadString;
}
@property(nonatomic,strong) NSMutableArray *p_dataArray;
@property(nonatomic,strong)LoadFaild *p_loadFaild;
@property(nonatomic,strong)NSString *p_loadString;
@end

@implementation DemandDetailView
@synthesize p_dataArray = m_dataArray;
@synthesize p_loadFaild = m_loadFaild;
@synthesize p_loadString = m_loadString;
@synthesize p_demandWebView = m_demandWebView;
@synthesize p_demandDetailDelegate = m_demandDetailDelegate;

-(void)dealloc
{
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if (self) {
        self.p_loadFaild = [[LoadFaild alloc]initWithFrame:CGRectMake(0, 0, kMainScreen_Width, kMainScreen_Height-64)];
        m_loadFaild.p_loadFaildDelegate = self;
        m_loadFaild.hidden = YES;
        [self addSubview:m_loadFaild];
    }
    return self;
}

-(void)loadServiceDemandDetail:(NSString *)URL
{
    self.p_dataArray = [[NSMutableArray alloc]init];
    //从网络加载
    [m_demandWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
}

-(void)sendDemandResultString:(NSString *)resultStr
{
    [m_demandWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"sendAddress(%@)",resultStr]];
}

#pragma mark - LoadFaildDelegate
-(void)reloadTheWebView:(id)sender
{
    [m_demandWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:m_loadString]]];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // starting the load, show the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    m_loadFaild.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // finished loading, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
    if (m_demandDetailDelegate && [m_demandDetailDelegate respondsToSelector:@selector(webviewTitleFormDemand:)]) {
        [m_demandDetailDelegate webviewTitleFormDemand:title];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // load error, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    m_loadFaild.hidden = NO;
}

//在这个回调方法中，我们会得到服务器传过来的数据request，我们可以判断穿过来的参数，然后进行判断调用oc的方法
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"request:%@",request.URL.absoluteString);
    NSRange strRange = [request.URL.absoluteString rangeOfString:@"action=index"];
    NSRange strRange2 = [request.URL.absoluteString rangeOfString:@"/popupAddressPickerWithSelected"];
    NSRange strRange3 = [request.URL.absoluteString rangeOfString:@"/init"];
    self.p_loadString = request.URL.absoluteString;
    NSRange blank = [request.URL.absoluteString rangeOfString:@"about:blank"];
    NSRange network = [request.URL.absoluteString rangeOfString:@"isNetworkConnected"];
    if (blank.location != NSNotFound){
        return NO;
    }
    else if (network.location != NSNotFound){
        if (AppManage.p_noNetWork) {
            [m_demandWebView stringByEvaluatingJavaScriptFromString:@"sendNetState(false)"];
            [[YBProgressShow defaultProgress] showText:@"无网络"
                                                InMode:MBProgressHUDModeText
                                 OriginViewEnableTouch:NO
                                    HideAfterDelayTime:1.5f];
        }
        return NO;
    }
    else if (strRange3.location != NSNotFound) {
        if (m_demandDetailDelegate && [m_demandDetailDelegate respondsToSelector:@selector(demandDetailSelect:)]) {
            [m_demandDetailDelegate demandDetailSelect:nil];
        }
        return NO;
    }

    else if (strRange.location != NSNotFound && strRange2.location == NSNotFound ){
        if (m_demandDetailDelegate && [m_demandDetailDelegate respondsToSelector:@selector(showDemandFirst:andMethod:)]) {
            [m_demandDetailDelegate showDemandFirst:request.URL.absoluteString andMethod:1];
        }
        return NO;
    }
    else if (strRange2.location != NSNotFound) {
        //获取数据
        NSArray *arr = [request.URL.absoluteString componentsSeparatedByString:@"/"];
        NSString *cityStr = [arr lastObject];
        if (m_demandDetailDelegate && [m_demandDetailDelegate respondsToSelector:@selector(selectDemandAddress:)]) {
            [m_demandDetailDelegate selectDemandAddress:cityStr];
        }
        return NO;
    }
    return YES;
}


@end
