//
//  MeDetailView.m
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "MeDetailView.h"
#import "ProvinceModel.h"
#import "CityModel.h"
#import "LoadFaild.h"
#import "AppContextManager.h"
@interface MeDetailView ()<LoadFaildDelegate>
{
    NSMutableArray *m_dataArray;
    LoadFaild *m_loadFaild;
    NSString *m_loadString;
}
@property(nonatomic,strong) NSMutableArray *p_dataArray;
@property(nonatomic,strong) LoadFaild *p_loadFaild;
@property(nonatomic,strong) NSString *p_loadString;
@end
@implementation MeDetailView
@synthesize p_dataArray = m_dataArray;
@synthesize p_loadFaild = m_loadFaild;
@synthesize p_loadString = m_loadString;
@synthesize p_meWebView = m_meWebView;
@synthesize p_meDetailDelegate = m_meDetailDelegate;

-(void)dealloc
{
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if (self) {
        self.p_loadFaild = [[LoadFaild alloc]initWithFrame:CGRectMake(0, 0, kMainScreen_Width, kMainScreen_Height-64)];
        m_loadFaild.p_loadFaildDelegate = self;
        m_loadFaild.hidden = YES;
        [self addSubview:m_loadFaild];
    }
    return self;
}

-(void)loadServiceMeDetail:(NSString *)URL
{
    self.p_dataArray = [[NSMutableArray alloc]init];
    //从网络加载
    [m_meWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
}

-(void)sendResultString:(NSString *)resultStr
{
    [m_meWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"sendAddress(%@)",resultStr]];
}

#pragma mark - LoadFaildDelegate
-(void)reloadTheWebView:(id)sender
{
    [m_meWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:m_loadString]]];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // starting the load, show the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    m_loadFaild.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // finished loading, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
    if (m_meDetailDelegate && [m_meDetailDelegate respondsToSelector:@selector(webviewTitleFormMe:)]) {
        [m_meDetailDelegate webviewTitleFormMe:title];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // load error, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    m_loadFaild.hidden = NO;
}

//在这个回调方法中，我们会得到服务器传过来的数据request，我们可以判断穿过来的参数，然后进行判断调用oc的方法
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"request:%@",request.URL.absoluteString);
    NSRange strRange = [request.URL.absoluteString rangeOfString:@"firstview"];
    NSRange strRange2 = [request.URL.absoluteString rangeOfString:@"/popupAddressPickerWithSelected"];
    NSRange strRange3 = [request.URL.absoluteString rangeOfString:@"showImg"];
    NSRange strRange4 = [request.URL.absoluteString rangeOfString:@"hideImg"];
    NSRange strRange5 = [request.URL.absoluteString rangeOfString:@"saveImage"];
    NSRange strRange6 = [request.URL.absoluteString rangeOfString:@"flag=goback"];
    NSRange strRange7 = [request.URL.absoluteString rangeOfString:@"/init"];
    
    self.p_loadString = request.URL.absoluteString;
    NSRange blank = [request.URL.absoluteString rangeOfString:@"about:blank"];
    NSRange network = [request.URL.absoluteString rangeOfString:@"isNetworkConnected"];
    if (blank.location != NSNotFound){
        return NO;
    }
    else if (network.location != NSNotFound){
        if (AppManage.p_noNetWork) {
            [m_meWebView stringByEvaluatingJavaScriptFromString:@"sendNetState(false)"];
            [[YBProgressShow defaultProgress] showText:@"无网络"
                                                InMode:MBProgressHUDModeText
                                 OriginViewEnableTouch:NO
                                    HideAfterDelayTime:1.5f];
        }
        return NO;
    }
    else if (strRange7.location != NSNotFound){
        if (m_meDetailDelegate && [m_meDetailDelegate respondsToSelector:@selector(meDetaillSelect:)]) {
            [m_meDetailDelegate meDetaillSelect:nil];
        }
        return NO;
    }
    else if (strRange3.location != NSNotFound){
        if (m_meDetailDelegate && [m_meDetailDelegate respondsToSelector:@selector(imageHiddenMeDetail:)]) {
            [m_meDetailDelegate imageHiddenMeDetail:YES];
        }
        return NO;
    }
    else if (strRange4.location != NSNotFound){
        if (m_meDetailDelegate && [m_meDetailDelegate respondsToSelector:@selector(imageHiddenMeDetail:)]) {
            [m_meDetailDelegate imageHiddenMeDetail:NO];
        }
        return NO;
    }
    else if (strRange5.location != NSNotFound){
        if (m_meDetailDelegate && [m_meDetailDelegate respondsToSelector:@selector(saveMeDetailImage:)]) {
            NSArray *arr = [request.URL.absoluteString componentsSeparatedByString:@"/saveImage/"];
            [m_meDetailDelegate saveMeDetailImage:[arr lastObject]];
        }
        return NO;
    }
    else if (strRange6.location != NSNotFound){
        if (m_meDetailDelegate && [m_meDetailDelegate respondsToSelector:@selector(backRefresh:)]) {
            [m_meDetailDelegate backRefresh:request.URL.absoluteString];
        }
        return YES;
    }
    else if (strRange.location != NSNotFound && strRange2.location == NSNotFound){
        if (m_meDetailDelegate && [m_meDetailDelegate respondsToSelector:@selector(showMeFirst:)]) {
            [m_meDetailDelegate showMeFirst:request.URL.absoluteString];
        }
        return NO;
    }
    else if (strRange2.location != NSNotFound) {
        //获取数据
        NSArray *arr = [request.URL.absoluteString componentsSeparatedByString:@"/"];
        NSString *cityStr = [arr lastObject];
        if (m_meDetailDelegate && [m_meDetailDelegate respondsToSelector:@selector(selectAddress:)]) {
            [m_meDetailDelegate selectAddress:cityStr];
        }
        return NO;
    }
    else
    {
        if (m_meDetailDelegate && [m_meDetailDelegate respondsToSelector:@selector(gobackNormal:)]) {
            [m_meDetailDelegate gobackNormal:nil];
        }
        return YES;
    }
    
}

@end
