//
//  MeDetailView.h
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MeDetailDelegate <NSObject>

-(void)webviewTitleFormMe:(NSString *)titleStr;
-(void)selectAddress:(NSString *)cityStr;
-(void)showMeFirst:(NSString *)url;
-(void)imageHiddenMeDetail:(BOOL)isHidden;
-(void)saveMeDetailImage:(NSString *)url;
-(void)backRefresh:(NSString *)url;
-(void)gobackNormal:(id)sender;
-(void)meDetaillSelect:(id)sender;
@end

@interface MeDetailView : UIView<UIWebViewDelegate>
{
    UIWebView *m_meWebView;
    __weak id<MeDetailDelegate> m_meDetailDelegate;
}
@property (nonatomic,strong) IBOutlet UIWebView *p_meWebView;
@property (nonatomic,weak) id<MeDetailDelegate> p_meDetailDelegate;

-(void)loadServiceMeDetail:(NSString *)URL;
-(void)sendResultString:(NSString *)resultStr;
@end
