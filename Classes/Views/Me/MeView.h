//
//  MeView.h
//  WJService
//
//  Created by 邵 旭 on 15/3/17.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MeDelegate <NSObject>

-(void)webviewTitleFormMeFirst:(NSString *)titleStr;
-(void)showMeDetail:(NSString *)detailUrl;
@end

@interface MeView : UIView<UIWebViewDelegate>
{
    UIWebView *m_meWebView;
    __weak id<MeDelegate> m_meDelegate;
}
@property (nonatomic,strong) IBOutlet UIWebView *p_meWebView;
@property (nonatomic,weak) id<MeDelegate> p_meDelegate;

-(void)loadMeWeb:(NSString *)URL;

@end
