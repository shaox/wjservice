//
//  LoadRefresh.m
//  WJService
//
//  Created by 邵 旭 on 15/4/1.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "LoadRefresh.h"

@interface LoadRefresh ()
{
    UIImageView *m_loadIma;
}
@property(nonatomic,strong)UIImageView *p_loadIma;
@end

@implementation LoadRefresh
@synthesize p_loadIma = m_loadIma;

-(void)dealloc
{
    
}

-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self createView];
    }
    return self;
}

-(void)createView
{
//    self.p_loadIma = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico-loads.png"]];
//    m_loadIma.frame = CGRectMake((kMainScreen_Width), <#CGFloat y#>, <#CGFloat width#>, <#CGFloat height#>)
}

-(void)begin
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    rotationAnimation.duration = 0.5f;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = FLT_MAX;
    
//    [m_activeView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

@end
