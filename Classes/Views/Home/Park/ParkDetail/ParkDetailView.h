//
//  ParkDetailView.h
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ParkDetailDelegate <NSObject>

-(void)webviewTitleFormPark:(NSString *)titleStr;
-(void)showDemandFirst:(NSString *)url;
-(void)imageHidden:(BOOL)isHidden;
-(void)saveDetailImage:(NSString *)url;
-(void)navColor:(NSString *)url;
@end

@interface ParkDetailView : UIView<UIWebViewDelegate>
{
    UIWebView *m_parkDetailWebView;
    __weak id<ParkDetailDelegate> m_parkDetailDelegate;
}
@property (nonatomic,strong) IBOutlet UIWebView *p_parkDetailWebView;
@property (nonatomic,weak) id<ParkDetailDelegate> p_parkDetailDelegate;

-(void)loadServiceParkDetail:(NSString *)URL;

@end
