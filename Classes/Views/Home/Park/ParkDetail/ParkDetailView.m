//
//  ParkDetailView.m
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "ParkDetailView.h"
#import "LoadFaild.h"
#import "AppContextManager.h"

@interface ParkDetailView ()<LoadFaildDelegate>
{
    LoadFaild *m_loadFaild;
    NSString *m_loadString;
}
@property(nonatomic,strong) LoadFaild *p_loadFaild;
@property(nonatomic,strong) NSString *p_loadString;
@end

@implementation ParkDetailView
@synthesize p_loadFaild = m_loadFaild;
@synthesize p_loadString = m_loadString;
@synthesize p_parkDetailWebView = m_parkDetailWebView;
@synthesize p_parkDetailDelegate = m_parkDetailDelegate;

-(void)dealloc
{
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if (self) {

        self.p_loadFaild = [[LoadFaild alloc]initWithFrame:CGRectMake(0, 0, kMainScreen_Width, kMainScreen_Height-64)];
        m_loadFaild.p_loadFaildDelegate = self;
        m_loadFaild.hidden = YES;
        [self addSubview:m_loadFaild];
    }
    return self;
}

-(void)loadServiceParkDetail:(NSString *)URL
{
    //从网络加载
    [m_parkDetailWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
}

#pragma mark - LoadFaildDelegate
-(void)reloadTheWebView:(id)sender
{
    [m_parkDetailWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:m_loadString]]];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // starting the load, show the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    m_loadFaild.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // finished loading, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
    if (m_parkDetailDelegate && [m_parkDetailDelegate respondsToSelector:@selector(webviewTitleFormPark:)]) {
        [m_parkDetailDelegate webviewTitleFormPark:title];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // load error, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    m_loadFaild.hidden = NO;
}

//在这个回调方法中，我们会得到服务器传过来的数据request，我们可以判断穿过来的参数，然后进行判断调用oc的方法
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"request:%@",request.URL.absoluteString);
    NSRange strRange = [request.URL.absoluteString rangeOfString:@"firstview"];
    NSRange strRange2 = [request.URL.absoluteString rangeOfString:@"showImg"];
    NSRange strRange3 = [request.URL.absoluteString rangeOfString:@"hideImg"];
    NSRange strRange4 = [request.URL.absoluteString rangeOfString:@"saveImage"];
    
    self.p_loadString = request.URL.absoluteString;
    NSRange blank = [request.URL.absoluteString rangeOfString:@"about:blank"];
    NSRange network = [request.URL.absoluteString rangeOfString:@"isNetworkConnected"];
    if (blank.location != NSNotFound){
        return NO;
    }
    else if (network.location != NSNotFound){
        if (AppManage.p_noNetWork) {
            [m_parkDetailWebView stringByEvaluatingJavaScriptFromString:@"sendNetState(false)"];
            [[YBProgressShow defaultProgress] showText:@"无网络"
                                                InMode:MBProgressHUDModeText
                                 OriginViewEnableTouch:NO
                                    HideAfterDelayTime:1.5f];
        }
        return NO;
    }
    else if (strRange.location != NSNotFound){
        if (m_parkDetailDelegate && [m_parkDetailDelegate respondsToSelector:@selector(showDemandFirst:)]) {
            [m_parkDetailDelegate showDemandFirst:request.URL.absoluteString];
        }
        return NO;
    }
    else if (strRange2.location != NSNotFound){
        if (m_parkDetailDelegate && [m_parkDetailDelegate respondsToSelector:@selector(imageHidden:)]) {
            [m_parkDetailDelegate imageHidden:YES];
        }
        return NO;
    }
    else if (strRange3.location != NSNotFound){
        if (m_parkDetailDelegate && [m_parkDetailDelegate respondsToSelector:@selector(imageHidden:)]) {
            [m_parkDetailDelegate imageHidden:NO];
        }
        return NO;
    }
    else if (strRange4.location != NSNotFound){
        if (m_parkDetailDelegate && [m_parkDetailDelegate respondsToSelector:@selector(saveDetailImage:)]) {
            NSArray *arr = [request.URL.absoluteString componentsSeparatedByString:@"/saveImage/"];
            [m_parkDetailDelegate saveDetailImage:[arr lastObject]];
        }
        return NO;
    }
    else{
        if (m_parkDetailDelegate && [m_parkDetailDelegate respondsToSelector:@selector(navColor:)]) {
            [m_parkDetailDelegate navColor:request.URL.absoluteString];
        }
        return YES;
    }
}


@end
