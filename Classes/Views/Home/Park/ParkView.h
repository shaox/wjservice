//
//  ParkView.h
//  WJService
//
//  Created by 邵 旭 on 15-3-5.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ParkDelegate <NSObject>

-(void)showParkDetail:(NSString *)parkUrl;
@end

@interface ParkView : UIView<UIWebViewDelegate>
{
    UIWebView *m_parkWebView;
    __weak id<ParkDelegate> m_parkDelegate;
}
@property(nonatomic,strong)IBOutlet UIWebView *p_parkWebView;
@property(nonatomic,weak) id<ParkDelegate> p_parkDelegate;

-(void)loadParkWeb:(NSString *)URL;

@end
