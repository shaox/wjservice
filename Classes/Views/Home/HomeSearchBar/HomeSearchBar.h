//
//  HomeSearchBar.h
//  WJService
//
//  Created by 邵 旭 on 15/3/20.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeSearchDelegate <NSObject>

-(void)menuClick:(id)sender;

@end

@interface HomeSearchBar : UISearchBar<UITextFieldDelegate>
{
    UIButton *m_menuBtn;
    UILabel *m_lbl;
    
    __weak id<HomeSearchDelegate> m_homeSearchDelegate;
}
@property(nonatomic,strong) UIButton *p_menuBtn;
@property(nonatomic,strong) UILabel *p_lbl;
@property(nonatomic,weak) id<HomeSearchDelegate> p_homeSearchDelegate;


@end
