//
//  HomeSearchBar.m
//  WJService
//
//  Created by 邵 旭 on 15/3/20.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "HomeSearchBar.h"

#define kBtnTitleColor [UIColor colorWithRed:234/255.0f green:115/255.0f blue:79/255.0f alpha:1.0f]
#define kBtnTitle @"取消"
#define kMenuWidth 55
#define kMenuheight 44
#define kSearchWidth kMainScreen_Width-75


@implementation HomeSearchBar
@synthesize p_menuBtn = m_menuBtn;
@synthesize p_lbl = m_lbl;
@synthesize p_homeSearchDelegate = m_homeSearchDelegate;

-(void)dealloc
{
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!m_menuBtn) {
        self.p_menuBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [m_menuBtn setFrame:CGRectMake(4, 0, 55, 44)];
        m_menuBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        m_menuBtn.tag = 100;
        [m_menuBtn setImage:[UIImage imageNamed:@"jt-search.png"] forState:UIControlStateNormal];
        [m_menuBtn setTitle:@"园区" forState:UIControlStateNormal];
        [m_menuBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        m_menuBtn.hidden = YES;
        [m_menuBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 44, 0, -4)];
        [m_menuBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -24, 0, 0)];
        [m_menuBtn addTarget:self action:@selector(menuClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:m_menuBtn];
    }
    self.autoresizesSubviews = YES;
    if (IsIOS7) {
        [self viewForsearch:[[self.subviews objectAtIndex:0]subviews]];
    }
    else{
        [self viewForsearch:self.subviews];
    }
}


-(void)viewForsearch:(NSArray *)arr
{
    for (id view in arr) {
        if ([view isKindOfClass:[UITextField class]]) {
            UITextField *searchField = (UITextField *)view;
            searchField.delegate = self;
            searchField.borderStyle = UITextBorderStyleNone;
            if (searchField.isEditing) {
                m_menuBtn.hidden = NO;
                [searchField setFrame:CGRectMake(65,6.5f,kSearchWidth-44,31)];
                searchField.placeholder = @"输入关键字，发现好资源";
            }else{
                m_menuBtn.hidden = YES;
                searchField.placeholder = @"搜索";
            }
        }
        else if ([view isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton *)view;
            if (btn.tag == 100) {
                [m_menuBtn setFrame:CGRectMake(4, 0, kMenuWidth, kMenuheight)];
            }else{
                btn.tag = 101;
                [btn setTitleColor:kBtnTitleColor forState:UIControlStateNormal];
                [btn setTitle:kBtnTitle forState:UIControlStateNormal];
            }
        }
        else if ([view isKindOfClass:[UIView class]] && IsIOS7){
            UIView *newView = (UIView *)view;
            if (!m_lbl) {
                self.p_lbl = [[UILabel alloc]init];
                if (newView.frame.origin.y<0) {
                    m_lbl.frame = CGRectMake(8, 6.5f-newView.frame.origin.y, kMainScreen_Width-60, 31);
                }else{
                    m_lbl.frame = CGRectMake(8, 6.5f-newView.frame.origin.y, kMainScreen_Width-16, 31);
                }
                m_lbl.backgroundColor = [UIColor whiteColor];
                m_lbl.layer.masksToBounds = YES;
                m_lbl.layer.cornerRadius = 5;
                [newView addSubview:m_lbl];
            }else{
                if (newView.frame.origin.y<0) {
                    m_lbl.frame = CGRectMake(8, 6.5f-newView.frame.origin.y, kMainScreen_Width-60, 31);
                }else{
                    m_lbl.frame = CGRectMake(8, 6.5f-newView.frame.origin.y, kMainScreen_Width-16, 31);
                }
            }
        }
    }
}

-(void)menuClick:(id)sender
{
    if (m_homeSearchDelegate && [m_homeSearchDelegate respondsToSelector:@selector(menuClick:)]) {
        [m_homeSearchDelegate menuClick:nil];
    }
}

@end
