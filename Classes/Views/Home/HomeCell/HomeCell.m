//
//  HomeCell.m
//  WJService
//
//  Created by 邵 旭 on 15/3/13.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "HomeCell.h"
#import "AppContextManager.h"
#define Originx 9.5f
#define Originy 9.5f
#define Width   (kMainScreen_Width/2)-15
#define Height  (kHomeCellHeightFor4-130)/3
#define LogoHeight  61
#define kBig 10/9.8f
#define kSmall 0.98f

#define LogoImage @"ico-logo.png"
#define LogoImageIphone @"bg-logo.png"
#define LogoTextIphone @"bg-logoText.png"
#define ParkImage @"image-park.png"
#define ParkIco @"ico-park.png"
#define SkillImage @"image-skill.png"
#define SkillIco @"ico-skill.png"
#define TalentImage @"image-Talent.png"
#define TalentIco @"ico-Talent.png"
#define CapitalImage @"image-Capital.png"
#define CapitalIco @"ico-Capital.png"
#define ClassImage @"image-Class.png"
#define ClassIco @"ico-Class.png"
#define ExpertsImage @"image-Experts.png"
#define ExpertsIco @"ico-Experts.png"
#define MechanismImage @"image-mechanism.png"
#define MechanismIco @"ico-Mechanism.png"


@interface HomeCell ()
{
    UIImageView *m_logoImageView;        //logo
    UIButton *m_parkBtn;            //园区
    UIButton *m_skillBtn;                 //技术
    UIImageView *m_skillImgView;
    UIImageView *m_skillIcoView;
    UIButton *m_talentBtn;          //人才
    UIButton *m_capitalBtn;         //资本
    UIButton *m_classBtn;           //课程
    UIButton *m_expertsBtn;         //专家
    UIButton *m_mechanismBtn;       //机构
    
    BOOL m_isIphone;
}
@property (nonatomic,strong) UIImageView *p_logoImageView;
@property (nonatomic,strong) UIButton *p_parkBtn;
@property (nonatomic,strong) UIButton *p_skillBtn;
@property (nonatomic,strong) UIImageView *p_skillImgView;
@property (nonatomic,strong) UIImageView *p_skillIcoView;
@property (nonatomic,strong) UIButton *p_talentBtn;
@property (nonatomic,strong) UIButton *p_capitalBtn;
@property (nonatomic,strong) UIButton *p_classBtn;
@property (nonatomic,strong) UIButton *p_expertsBtn;
@property (nonatomic,strong) UIButton *p_mechanismBtn;
@property (nonatomic,assign) BOOL p_isIphone;
@end

@implementation HomeCell
@synthesize p_logoImageView = m_logoImageView;
@synthesize p_parkBtn = m_parkBtn;
@synthesize p_skillBtn = m_skillBtn;
@synthesize p_skillImgView = m_skillImgView;
@synthesize p_skillIcoView = m_skillIcoView;
@synthesize p_talentBtn = m_talentBtn;
@synthesize p_capitalBtn = m_capitalBtn;
@synthesize p_classBtn = m_classBtn;
@synthesize p_expertsBtn = m_expertsBtn;
@synthesize p_mechanismBtn = m_mechanismBtn;
@synthesize p_isIphone = m_isIphone;
@synthesize p_homeCellDelegate = m_homeCellDelegate;

-(void)dealloc
{
    
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)createHomeCell
{
//    self.backgroundColor = [UIColor colorWithRed:240/255.0f green:240/255.0f blue:240/255.0f alpha:1.0f];
//    self.backgroundColor = [UIColor clearColor];
    //TODO 目前url地址为空，可扩展
    self.p_isIphone = [CommonDeal checkDevice];
    
    CGFloat logo  = LogoHeight;
    CGFloat btnWidth = Width;
    CGFloat btnHeight = Height;
    CGFloat x = Originx;
    CGFloat y = Originy;
    if (!m_isIphone) {
        logo = 100.0f;
        btnWidth = 361.0f;
        btnHeight = 233.0f;
        x = 15.0f;
        y = 15.0f;
    }
    [self createWJLogo:CGRectMake(0, 0, kMainScreen_Width, logo)];    //无界logo广告
    [self createPark:CGRectMake(x, y+logo, btnWidth, btnHeight)];               //园区
    [self createSkill:CGRectMake(2*x+btnWidth, y+logo, btnWidth, btnHeight) andIcoFrame:CGRectMake(2*x+btnWidth, y+btnHeight*2.33/3.33f+logo, btnWidth, btnHeight/3.33f)];      //技术
    [self createTalent:CGRectMake(x, 2*y+btnHeight+logo, btnWidth, btnHeight)];    //人才
    [self createCapital:CGRectMake(2*x+btnWidth, 2*y+btnHeight+logo, btnWidth, btnHeight)];   //资本
    [self createClass:CGRectMake(x, logo+3*y+2*btnHeight, btnWidth, btnHeight/2+y)];     //课程
    [self createExperts:CGRectMake(x, logo+5*y+5*btnHeight/2, btnWidth, btnHeight/2+y)];   //专家
    [self createMechanism:CGRectMake(2*x+btnWidth, logo+3*y+2*btnHeight, btnWidth, btnHeight+3*y)]; //机构
}

-(void)createWJLogo:(CGRect)logoFrame
{
    self.p_logoImageView = [[UIImageView alloc]initWithFrame:logoFrame];
    if (m_isIphone) {
        m_logoImageView.image = [UIImage imageNamed:LogoImageIphone];
        [self addSubview:m_logoImageView];
        UIImageView *textIma = [[UIImageView alloc]initWithFrame:CGRectMake((kMainScreen_Width-280)/2.0f, (logoFrame.size.height-24)/2.0f, 280, 24)];
        textIma.image = [UIImage imageNamed:LogoTextIphone];
        textIma.contentMode = UIViewContentModeScaleToFill;
        [self addSubview:textIma];
    }
    else{
        m_logoImageView.image = [UIImage imageNamed:LogoImage];
        [self addSubview:m_logoImageView];
    }
}

-(void)homeBtnClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
//    CGAffineTransform transform = btn.transform;
    CGAffineTransform transform = CGAffineTransformScale(btn.transform, kSmall, kSmall);
    btn.transform = transform;
}

-(void)homeBtnOutClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
//    CGAffineTransform transform = btn.transform;
    CGAffineTransform transform = CGAffineTransformScale(btn.transform, kBig, kBig);
    btn.transform = transform;
}

-(void)touchOver:(UIButton *)btn
{
    if (m_isIphone)
    {
//        CGAffineTransform transform = btn.transform;
        CGAffineTransform transform = CGAffineTransformScale(btn.transform, kBig, kBig);
        btn.transform = transform;
    }
}

#pragma mark - park button
-(void)createPark:(CGRect)btnFrame
{
    self.p_parkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    m_parkBtn.frame = btnFrame;
    [m_parkBtn setBackgroundImage:[UIImage imageNamed:ParkImage] forState:UIControlStateNormal];
    m_parkBtn.contentMode = UIViewContentModeScaleToFill;
    [m_parkBtn setImage:[UIImage imageNamed:ParkIco] forState:UIControlStateNormal];
    [m_parkBtn setImage:[UIImage imageNamed:ParkIco] forState:UIControlStateHighlighted];
    if (m_isIphone)
    {
        [m_parkBtn addTarget:self action:@selector(homeBtnClick:) forControlEvents:UIControlEventTouchDown];
        [m_parkBtn addTarget:self action:@selector(homeBtnOutClick:) forControlEvents:UIControlEventTouchUpOutside];
    }
    [m_parkBtn addTarget:self action:@selector(parkBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:m_parkBtn];
}

-(void)parkBtnClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self touchOver:btn];
    if (m_homeCellDelegate && [m_homeCellDelegate respondsToSelector:@selector(clickWithHomeAction:)]) {
        [m_homeCellDelegate clickWithHomeAction:clickPark];
    }
}

#pragma mark - skill button
-(void)createSkill:(CGRect)btnFrame andIcoFrame:(CGRect)icoFrame
{
    self.p_skillBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    m_skillBtn.frame = btnFrame;
    [m_skillBtn setTitle:@"-" forState:UIControlStateNormal];
    [m_skillBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    if (m_isIphone)
    {
        [m_skillBtn addTarget:self action:@selector(skillHomeBtnClick:) forControlEvents:UIControlEventTouchDown];
        [m_skillBtn addTarget:self action:@selector(skillHomeBtnOutClick:) forControlEvents:UIControlEventTouchUpOutside];
    }
    [m_skillBtn addTarget:self action:@selector(skillBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.p_skillImgView = [[UIImageView alloc]initWithFrame:btnFrame];
    m_skillImgView.image = [UIImage imageNamed:SkillImage];
    m_skillImgView.contentMode = UIViewContentModeScaleToFill;
    [self addSubview:m_skillImgView];
    
    self.p_skillIcoView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:SkillIco]];
    m_skillIcoView.frame = icoFrame;
    m_skillIcoView.contentMode = UIViewContentModeScaleToFill;
    m_skillIcoView.backgroundColor = jnColor;
    [self addSubview:m_skillIcoView];
    
    [self addSubview:m_skillBtn];
}

-(void)skillBtnClick:(id)sender
{
    if (m_isIphone)
    {
        UIButton *btn = (UIButton *)sender;
//        CGAffineTransform transform = btn.transform;
//        CGAffineTransform transform2 = m_skillImgView.transform;
//        CGAffineTransform transform3 = m_skillIcoView.transform;
        CGAffineTransform transform = CGAffineTransformScale(btn.transform, kBig, kBig);
        CGAffineTransform transform2 = CGAffineTransformScale(m_skillImgView.transform, kBig, kBig);
        CGAffineTransform transform3 = CGAffineTransformScale(m_skillIcoView.transform, kBig, kBig);
        btn.transform = transform;
        m_skillImgView.transform = transform2;
        m_skillIcoView.transform = transform3;
    }

    if (m_homeCellDelegate && [m_homeCellDelegate respondsToSelector:@selector(clickWithHomeAction:)]) {
        [m_homeCellDelegate clickWithHomeAction:clickSkill];
    }
}

-(void)skillHomeBtnClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
//    CGAffineTransform transform = btn.transform;
//    CGAffineTransform transform2 = m_skillImgView.transform;
//    CGAffineTransform transform3 = m_skillIcoView.transform;
    CGAffineTransform transform = CGAffineTransformScale(btn.transform, kSmall, kSmall);
    CGAffineTransform transform2 = CGAffineTransformScale(m_skillImgView.transform, kSmall, kSmall);
    CGAffineTransform transform3 = CGAffineTransformScale(m_skillIcoView.transform, kSmall, kSmall);
    btn.transform = transform;
    m_skillImgView.transform = transform2;
    m_skillIcoView.transform = transform3;
}

-(void)skillHomeBtnOutClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
//    CGAffineTransform transform = btn.transform;
//    CGAffineTransform transform2 = m_skillImgView.transform;
//    CGAffineTransform transform3 = m_skillIcoView.transform;
    CGAffineTransform transform = CGAffineTransformScale(btn.transform, kBig, kBig);
    CGAffineTransform transform2 = CGAffineTransformScale(m_skillImgView.transform, kBig, kBig);
    CGAffineTransform transform3 = CGAffineTransformScale(m_skillIcoView.transform, kBig, kBig);
    btn.transform = transform;
    m_skillImgView.transform = transform2;
    m_skillIcoView.transform = transform3;
}

#pragma mark - Talent button
-(void)createTalent:(CGRect)btnFrame
{
    self.p_talentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    m_talentBtn.frame = btnFrame;
    m_talentBtn.backgroundColor = rcColor;
    m_talentBtn.contentMode = UIViewContentModeScaleToFill;
    [m_talentBtn setImage:[UIImage imageNamed:TalentIco] forState:UIControlStateNormal];
    [m_talentBtn setImage:[UIImage imageNamed:TalentIco] forState:UIControlStateHighlighted];
    if (m_isIphone)
    {
        [m_talentBtn addTarget:self action:@selector(homeBtnClick:) forControlEvents:UIControlEventTouchDown];
        [m_talentBtn addTarget:self action:@selector(homeBtnOutClick:) forControlEvents:UIControlEventTouchUpOutside];
    }
    [m_talentBtn addTarget:self action:@selector(talentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:m_talentBtn];
}

-(void)talentBtnClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self touchOver:btn];
    if (m_homeCellDelegate && [m_homeCellDelegate respondsToSelector:@selector(clickWithHomeAction:)]) {
        [m_homeCellDelegate clickWithHomeAction:clickTalent];
    }
}

#pragma mark - Capital button
-(void)createCapital:(CGRect)btnFrame
{
    self.p_capitalBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    m_capitalBtn.frame = btnFrame;
    m_capitalBtn.backgroundColor = zbColor;
    m_capitalBtn.contentMode = UIViewContentModeScaleToFill;
    [m_capitalBtn setImage:[UIImage imageNamed:CapitalIco] forState:UIControlStateNormal];
    [m_capitalBtn setImage:[UIImage imageNamed:CapitalIco] forState:UIControlStateHighlighted];
    if (m_isIphone) {
        [m_capitalBtn addTarget:self action:@selector(homeBtnClick:) forControlEvents:UIControlEventTouchDown];
        [m_capitalBtn addTarget:self action:@selector(homeBtnOutClick:) forControlEvents:UIControlEventTouchUpOutside];
    }
    [m_capitalBtn addTarget:self action:@selector(capitalBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:m_capitalBtn];
}

-(void)capitalBtnClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self touchOver:btn];
    if (m_homeCellDelegate && [m_homeCellDelegate respondsToSelector:@selector(clickWithHomeAction:)]) {
        [m_homeCellDelegate clickWithHomeAction:clickCapital];
    }
}

#pragma mark - Class button
-(void)createClass:(CGRect)btnFrame
{
    self.p_classBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    m_classBtn.frame = btnFrame;
    m_classBtn.backgroundColor = ztColor;
    m_classBtn.contentMode = UIViewContentModeScaleToFill;
    [m_classBtn setImage:[UIImage imageNamed:ClassIco] forState:UIControlStateNormal];
    [m_classBtn setImage:[UIImage imageNamed:ClassIco] forState:UIControlStateHighlighted];
    if (m_isIphone) {
        [m_classBtn addTarget:self action:@selector(homeBtnClick:) forControlEvents:UIControlEventTouchDown];
        [m_classBtn addTarget:self action:@selector(homeBtnOutClick:) forControlEvents:UIControlEventTouchUpOutside];
    }
    [m_classBtn addTarget:self action:@selector(classBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:m_classBtn];
}

-(void)classBtnClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self touchOver:btn];
    if (m_homeCellDelegate && [m_homeCellDelegate respondsToSelector:@selector(clickWithHomeAction:)]) {
        [m_homeCellDelegate clickWithHomeAction:clickClass];
    }
}

#pragma mark - experts button
-(void)createExperts:(CGRect)btnFrame
{
    self.p_expertsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    m_expertsBtn.frame = btnFrame;
    m_expertsBtn.backgroundColor = zjColor;
    m_expertsBtn.contentMode = UIViewContentModeScaleToFill;
    [m_expertsBtn setImage:[UIImage imageNamed:ExpertsIco] forState:UIControlStateNormal];
    [m_expertsBtn setImage:[UIImage imageNamed:ExpertsIco] forState:UIControlStateHighlighted];
    if (m_isIphone) {
        [m_expertsBtn addTarget:self action:@selector(homeBtnClick:) forControlEvents:UIControlEventTouchDown];
        [m_expertsBtn addTarget:self action:@selector(homeBtnOutClick:) forControlEvents:UIControlEventTouchUpOutside];
    }
    [m_expertsBtn addTarget:self action:@selector(expertsBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:m_expertsBtn];
}

-(void)expertsBtnClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self touchOver:btn];
    if (m_homeCellDelegate && [m_homeCellDelegate respondsToSelector:@selector(clickWithHomeAction:)]) {
        [m_homeCellDelegate clickWithHomeAction:clickExperts];
    }
}

#pragma mark - Mechanism button
-(void)createMechanism:(CGRect)btnFrame
{
    self.p_mechanismBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    m_mechanismBtn.frame = btnFrame;
    m_mechanismBtn.contentMode = UIViewContentModeScaleToFill;
    [m_mechanismBtn setImage:[UIImage imageNamed:MechanismIco] forState:UIControlStateNormal];
    [m_mechanismBtn setImage:[UIImage imageNamed:MechanismIco] forState:UIControlStateHighlighted];
    [m_mechanismBtn setBackgroundImage:[UIImage imageNamed:MechanismImage] forState:UIControlStateNormal];
    [m_mechanismBtn setBackgroundImage:[UIImage imageNamed:MechanismImage] forState:UIControlStateHighlighted];
    if (m_isIphone)
    {
        [m_mechanismBtn addTarget:self action:@selector(homeBtnClick:) forControlEvents:UIControlEventTouchDown];
        [m_mechanismBtn addTarget:self action:@selector(homeBtnOutClick:) forControlEvents:UIControlEventTouchUpOutside];
    }
    [m_mechanismBtn addTarget:self action:@selector(mechanismBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:m_mechanismBtn];
}

-(void)mechanismBtnClick:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self touchOver:btn];
    if (m_homeCellDelegate && [m_homeCellDelegate respondsToSelector:@selector(clickWithHomeAction:)]) {
        [m_homeCellDelegate clickWithHomeAction:clickMechanism];
    }
}

@end
