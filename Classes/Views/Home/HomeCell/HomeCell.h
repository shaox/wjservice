//
//  HomeCell.h
//  WJService
//
//  Created by 邵 旭 on 15/3/13.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeCellDelegate <NSObject>

/*!创建首页视图
 \*param            无
 \*returns          无
 **/
-(void)clickWithHomeAction:(HomeAction)action;

@end

@interface HomeCell : UITableViewCell
{
    __weak id<HomeCellDelegate> m_homeCellDelegate;
}
@property(nonatomic,weak) id<HomeCellDelegate> p_homeCellDelegate;

/*!创建首页视图
 \*param            无
 \*returns          无
 **/
-(void)createHomeCell;

@end
