//
//  SearchResultCell.h
//  WJService
//
//  Created by 邵 旭 on 15/3/23.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchResultDelegate <NSObject>

-(void)resultWithUrl:(NSString *)url;
-(void)resultWithTitle:(NSString *)title;
@end

@interface SearchResultCell : UITableViewCell<UIWebViewDelegate>
{
    UIWebView *m_cellWebView;
    NSString *m_url;
    __weak id<SearchResultDelegate> m_searchResultDelegate;
}
@property(nonatomic,strong) UIWebView *p_cellWebView;
@property(nonatomic,strong) NSString *p_url;
@property(nonatomic,weak)id<SearchResultDelegate> p_searchResultDelegate;

-(void)loadServiceSearch:(NSString *)URL;
-(void)loadView;
@end
