//
//  SearchResultCell.m
//  WJService
//
//  Created by 邵 旭 on 15/3/23.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "SearchResultCell.h"
#import "LoadFaild.h"

@interface SearchResultCell ()<LoadFaildDelegate>
{
    LoadFaild *m_loadFaild;
    NSString *m_loadString;
}
@property(nonatomic,strong)LoadFaild *p_loadFaild;
@property(nonatomic,strong)NSString *p_loadString;
@end

@implementation SearchResultCell
@synthesize p_loadFaild = m_loadFaild;
@synthesize p_loadString = m_loadString;
@synthesize p_cellWebView = m_cellWebView;
@synthesize p_url = m_url;
@synthesize p_searchResultDelegate = m_searchResultDelegate;

-(void)dealloc
{
}

- (void)awakeFromNib {
    // Initialization code
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if (self) {

    }
    return self;
}

-(void)loadView
{
    if (!self.p_loadFaild) {
        self.p_loadFaild = [[LoadFaild alloc]initWithFrame:CGRectMake(0, 0, kMainScreen_Width, kMainScreen_Height-113)];
        m_loadFaild.p_loadFaildDelegate = self;
        m_loadFaild.hidden = YES;
        [self addSubview:m_loadFaild];
    }
}

#pragma mark - LoadFaildDelegate
-(void)reloadTheWebView:(id)sender
{
    [m_cellWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:m_loadString]]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)loadServiceSearch:(NSString *)URL
{
    if (!m_cellWebView) {
        self.p_cellWebView = [[UIWebView alloc]initWithFrame:self.frame];
        m_cellWebView.delegate = self;
        m_cellWebView.hidden = NO;
        [self.contentView addSubview:m_cellWebView];
    }
    //从网络加载
    [m_cellWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // starting the load, show the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    m_loadFaild.hidden = YES;
    m_cellWebView.hidden = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // finished loading, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];//获取当前页面的title
    if (m_searchResultDelegate && [m_searchResultDelegate respondsToSelector:@selector(resultWithTitle:)]) {
        [m_searchResultDelegate resultWithTitle:title];
    }

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // load error, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    m_loadFaild.hidden = NO;
    m_cellWebView.hidden = YES;
}

//在这个回调方法中，我们会得到服务器传过来的数据request，我们可以判断穿过来的参数，然后进行判断调用oc的方法
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"search:%@",request.URL.absoluteString);
    self.p_loadString = request.URL.absoluteString;
    NSRange strRange = [request.URL.absoluteString rangeOfString:@"/search"];
    if (strRange.location == NSNotFound){
        if (m_searchResultDelegate && [m_searchResultDelegate respondsToSelector:@selector(resultWithUrl:)]) {
            [m_searchResultDelegate resultWithUrl:request.URL.description];
        }
        return NO;
    }
    return YES;
}

@end
