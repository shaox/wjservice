//
//  SearchView.h
//  WJService
//
//  Created by 邵 旭 on 15/3/23.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SearchViewDelegate <NSObject>

-(void)webviewTitleFormSearch:(NSString *)title;
-(void)imageSearchHidden:(BOOL)isHidden;
-(void)saveSearchImage:(NSString *)url;
-(void)titles:(NSString *)url;
@end

@interface SearchView : UIView<UIWebViewDelegate>
{
    UIWebView *m_webView;
    __weak id<SearchViewDelegate> m_searchViewDelegate;
}
@property(nonatomic,strong) IBOutlet UIWebView *p_webView;
@property(nonatomic,weak) id<SearchViewDelegate> p_searchViewDelegate;

-(void)loadServiceResult:(NSString *)URL;

@end
