//
//  MenuView.m
//  WJService
//
//  Created by 邵 旭 on 15/3/21.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "MenuView.h"
#import "MenusCell.h"
@interface MenuView ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *m_menuTableView;
    NSMutableArray *m_titleArray;
}
@property (nonatomic,strong) UITableView *p_menuTableView;
@property (nonatomic,strong) NSMutableArray *p_titleArray;
@end

@implementation MenuView
@synthesize p_menuTableView = m_menuTableView;
@synthesize p_titleArray = m_titleArray;
@synthesize p_menuDelegate = m_menuDelegate;

-(void)dealloc
{
    
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self createMenuTableView:frame];
    }
    return self;
}

-(void)createMenuTableView:(CGRect)frame
{
    self.p_titleArray = [NSMutableArray arrayWithObjects:@"园区", @"技术", @"人才", @"资本", @"培训-专题", @"培训-专家", @"培训-机构",nil];
    UIImageView *bgIMG = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg-searchMenu.png"]];
    bgIMG.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
//    bgIMG.layer.borderColor = [[UIColor greenColor]CGColor];
//    bgIMG.layer.borderWidth = 2;
    [self addSubview:bgIMG];
    self.p_menuTableView = [[UITableView alloc] initWithFrame:CGRectMake(bgIMG.frame.origin.x, bgIMG.frame.origin.y+10, bgIMG.frame.size.width, bgIMG.frame.size.height-10) style:UITableViewStylePlain];
    m_menuTableView.delegate = self;
    m_menuTableView.dataSource = self;
    m_menuTableView.scrollEnabled = NO;
    m_menuTableView.backgroundColor = [UIColor clearColor];
    m_menuTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:m_menuTableView];
//    m_menuTableView.layer.borderColor = [[UIColor redColor]CGColor];
//    m_menuTableView.layer.borderWidth = 1;
    [m_menuTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return m_titleArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 39;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *SearchMenu = @"SearchMenuCell";
    MenusCell *cell = (MenusCell *)[tableView dequeueReusableCellWithIdentifier:@"SearchMenuCell"];
    if (cell == nil) {
        cell = [[MenusCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SearchMenu];
    }
    cell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    cell.p_textLbl.text = [m_titleArray objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (m_menuDelegate && [m_menuDelegate respondsToSelector:@selector(selectType:)]) {
        [m_menuDelegate selectType:[m_titleArray objectAtIndex:indexPath.row]];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
