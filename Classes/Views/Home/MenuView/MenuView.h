//
//  MenuView.h
//  WJService
//
//  Created by 邵 旭 on 15/3/21.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuDelegate <NSObject>

-(void)selectType:(NSString *)title;

@end

@interface MenuView : UIView
{
    __weak id<MenuDelegate> m_menuDelegate;
}
@property(nonatomic,weak) id<MenuDelegate> p_menuDelegate;
@end
