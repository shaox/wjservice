//
//  MenusCell.m
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "MenusCell.h"
#import "UIColor+Extension.h"
@interface MenusCell ()

@end

@implementation MenusCell
@synthesize p_textLbl = m_textLbl;

-(void)dealloc
{
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor color:[UIColor blackColor] withAlpha:0.8f];
        self.p_textLbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 2, 185, 35)];
        m_textLbl.textColor = [UIColor grayColor];
        
        [self.contentView addSubview:m_textLbl];
        self.selectionStyle = UITableViewCellEditingStyleNone;
    }
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        m_textLbl.textColor = textsColor;
        m_textLbl.backgroundColor = [UIColor colorWithRed:113.0/255.0 green:114.0/255.0 blue:116.0/255.0 alpha:1.0];
        [m_textLbl.layer setCornerRadius:3.0];
        [m_textLbl.layer setMasksToBounds:YES];
        
        
    }else{
        m_textLbl.textColor = [UIColor grayColor];
        m_textLbl.backgroundColor = [UIColor clearColor];
        [m_textLbl.layer setMasksToBounds:NO];
    }
}

@end
