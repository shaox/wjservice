//
//  LoadFaild.h
//  WJService
//
//  Created by 邵 旭 on 15/4/1.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoadFaildDelegate <NSObject>

-(void)reloadTheWebView:(id)sender;

@end

@interface LoadFaild : UIView
{
    __weak id<LoadFaildDelegate> m_loadFaildDelegate;
}
@property(nonatomic,weak)id<LoadFaildDelegate> p_loadFaildDelegate;

@end
