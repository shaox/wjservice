//
//  LoadFaild.m
//  WJService
//
//  Created by 邵 旭 on 15/4/1.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "LoadFaild.h"

@interface LoadFaild ()
{
    UIImageView *m_loadIma;
    UIButton *m_loadBtn;
}
@property(nonatomic,strong) UIImageView *p_loadIma;
@property(nonatomic,strong) UIButton *p_loadBtn;
@end

@implementation LoadFaild
@synthesize p_loadIma = m_loadIma;
@synthesize p_loadBtn = m_loadBtn;
@synthesize p_loadFaildDelegate = m_loadFaildDelegate;

-(void)dealloc
{
}

-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self createView];
    }
    return self;
}

-(void)createView
{
    self.backgroundColor = backBgColor;
    
    self.p_loadIma = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ico-loadFaild.png"]];
    m_loadIma.frame = CGRectMake((kMainScreen_Width-137)/2.0f, (self.frame.size.height-230)/2.0f, 137, 210);
    [self addSubview:m_loadIma];
    
    self.p_loadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    m_loadBtn.frame = CGRectMake(0, m_loadIma.frame.origin.y+210, kMainScreen_Width, 44);
    [m_loadBtn setTitle:@"重新加载" forState:UIControlStateNormal];
    [m_loadBtn setTitleColor:textsColor forState:UIControlStateNormal];
    [m_loadBtn addTarget:self action:@selector(reloadWeb:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:m_loadBtn];
}

-(void)reloadWeb:(id)sender
{
    if (m_loadFaildDelegate && [m_loadFaildDelegate respondsToSelector:@selector(reloadTheWebView:)]) {
        [m_loadFaildDelegate reloadTheWebView:nil];
    }
}


@end
