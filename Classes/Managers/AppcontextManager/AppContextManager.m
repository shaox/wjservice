//
//  AppContextManager.m
//  WangZhenVideo
//
//  Created by 邵 旭 on 14-10-14.
//  Copyright (c) 2014年 Tyrbl. All rights reserved.
//

#import "AppContextManager.h"
#import "AppDelegate.h"
static AppContextManager *g_appManager = nil;

@implementation AppContextManager
@synthesize p_currentUserM = m_currentUserM;
@synthesize p_isLogin = m_isLogin;
@synthesize p_noNetWork = m_noNetWork;
@synthesize p_servicePCArray = m_servicePCArray;
@synthesize p_allPCArray = m_allPCArray;

+ (AppContextManager *)shareInstanceManager
{
    @synchronized(self)
    {
        if (!g_appManager)
        {
            g_appManager = [[AppContextManager alloc] init];
        }
    }
    return g_appManager;
}

//存储图片并将
- (UIImage *)imgByUrl:(NSString *)imgUrl andPlaceholderImageName:(NSString *)placeholderImageName andFileName:(NSString *)filename
{
    if([CommonDeal jugementStringIsNil:imgUrl])
    {
        return [UIImage imageNamed:placeholderImageName];
    }else{
        NSString *imageName = [CommonDeal JAVAImageNameToIOSImageName:imgUrl andFlag:YES];
        NSString *imageUrl = [filename stringByAppendingPathComponent:imageName];
        UIImage *img = [UIImage imageWithContentsOfFile:imageUrl];
        if(img)
        {
            return img;
        }else{
            NSURL *url = [NSURL URLWithString:imgUrl];
            NSData *data = [NSData dataWithContentsOfURL:url];
            img = [UIImage imageWithData:data];
            if(img)
            {
                [[FileManager shareManager] createMsgImage:img andImageUrl:imgUrl andFileName:filename];//保存到本地数据库
                return img;
            }else{
                return [UIImage imageNamed:placeholderImageName];
            }
        }
    }
}

-(void)logoutApp
{
    [self logout];
}

-(void)logout
{
    self.p_currentUserM = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
