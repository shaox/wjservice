//
//  AppContextManager.h
//  WangZhenVideo
//
//  Created by 邵 旭 on 14-10-14.
//  Copyright (c) 2014年 Tyrbl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"
#import "FileManager.h"
@interface AppContextManager : NSObject
{
    UserModel *m_currentUserM;                      //当前usermodel的内容
    BOOL m_isLogin;
    BOOL m_noNetWork;
    NSArray *m_servicePCArray;
    NSArray *m_allPCArray;
}
@property(nonatomic,strong) UserModel *p_currentUserM;
@property(nonatomic,assign) BOOL p_isLogin;
@property(nonatomic,assign) BOOL p_noNetWork;
@property(nonatomic,strong) NSArray *p_servicePCArray;
@property(nonatomic,strong) NSArray *p_allPCArray;

/*!单利模式获取AppContextManagement对象
 *\param    param:          无
 *\returns  returns:        返回AppContextManagement对象
 */
+ (AppContextManager *)shareInstanceManager;

/*!根据url获取图片
 *\param    imgUrl:      图片的存储的位置的url
 *\param    placeholderImageName:      默认图片名字
 *\returns  returns:         图片
 */
- (UIImage *)imgByUrl:(NSString *)imgUrl andPlaceholderImageName:(NSString *)placeholderImageName andFileName:(NSString *)filename;

/*!登出，释放资源的操作
 *\param    param:      无
 *\returns  returns:        无
 */
-(void)logoutApp;
@end
