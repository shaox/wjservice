//
//  SettingManager.m
//  LR
//
//  Created by 邵 旭 on 14-6-4.
//  Copyright (c) 2014年 tyrbl. All rights reserved.
//

#import "SettingManager.h"

static SettingManager *m_settingManagement;

@interface SettingManager()
{
    NSDictionary *m_settingDic;
}

@property (nonatomic, strong) NSDictionary *m_settingDic;

@end

@implementation SettingManager
@synthesize m_settingDic;

- (void)dealloc
{

}

+ (SettingManager *)shareManagement
{
    @synchronized(self)
    {
        if (!m_settingManagement)
        {
            m_settingManagement = [[SettingManager alloc] init];
        }
    }
    return m_settingManagement;
}

- (id)init
{
    if (self = [super init])
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Setting" ofType:@"plist"];
        self.m_settingDic = [[NSDictionary alloc] initWithContentsOfFile:path];
    }
    return self;
}

- (NSString *)getBasicUrlWithKey:(NSString *)key
{
    return [[self.m_settingDic objectForKey:@"BasicSetting"] objectForKey:key];
}

- (NSString *)getAuthMessageWithKey:(NSString *)key
{
    return [[[self.m_settingDic objectForKey:@"BasicSetting"] objectForKey:@"auth"] objectForKey:key];
}


@end
