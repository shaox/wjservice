//
//  SettingManager.h
//  LR
//
//  Created by 邵 旭 on 14-6-4.
//  Copyright (c) 2014年 tyrbl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingManager : NSObject


/**单例模式，获取Setting管理类
*\param        param:无
*\returns      return:     无
*/
+ (SettingManager *)shareManagement;


/**获取服务器地址
 *\param        key:        键
 *\returns      return:     返回服务器地址
 */
- (NSString *)getBasicUrlWithKey:(NSString *)key;

/**获取鉴权信息
 *\param        key:        键
 *\returns      return:     返回鉴权信息，用户名或密码
 */
- (NSString *)getAuthMessageWithKey:(NSString *)key;
@end
