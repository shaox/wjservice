//
//  WujieApply.m
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "WujieApply.h"

static WujieApply *g_wujieApply;

@interface WujieApply()
{
    
}

@end

@implementation WujieApply

+ (WujieApply *)shareInstanceManager
{
    @synchronized(self)
    {
        if (!g_wujieApply)
        {
            g_wujieApply = [[WujieApply alloc] init];
        }
    }
    return g_wujieApply;
}

- (void)dismissWujieApply
{
    g_wujieApply = nil;
}

- (void)sendVerson:(NSString *)param andSuccessBlock:(SuccessBlock)sucBlock_ andFailedBlock:(FailedBlock)failedBlock_
{
    HttpWjService *httpWjService = [[HttpWjService alloc]init];
    [httpWjService queryVersonInfo:param andSuccessBlock:^(id backStr) {
        sucBlock_(backStr);
    } andFailedBlock:^(ErrorModel *errorM) {
        failedBlock_(errorM);
    }];
}

-(void)getCity:(BOOL)isNet andSuccessBlock:(SuccessBlock)sucBlock_ andFailedBlock:(FailedBlock)failedBlock_
{
    HttpWjService *httpWjService = [[HttpWjService alloc]init];
    [httpWjService queryCityData:isNet andSuccessBlock:^(id backStr) {
        NSMutableArray *dataArray = [[NSMutableArray alloc]init];
        NSDictionary *dic = (NSDictionary *)backStr;
        if([[dic valueForKeyPath:@"body"] isKindOfClass:[NSDictionary class]]){
            NSDictionary *newDic = [dic valueForKeyPath:@"body"];
            for (NSString *key in newDic.allKeys) {
                NSDictionary *valueDic = [newDic objectForKey:key];
                ProvinceModel *provinceM = [[ProvinceModel alloc]parseFromDic:valueDic];
                if (![provinceM.m_provinceID isEqualToString:@"567"]) {
                    [dataArray addObject:provinceM];
                }
            }
        }
        else if([[dic valueForKeyPath:@"body"] isKindOfClass:[NSArray class]]){
            NSArray *newArr = [dic valueForKeyPath:@"body"];
            for (int i=0; i<newArr.count; i++) {
                NSDictionary *cityDic = [newArr objectAtIndex:i];
                ProvinceModel *provinceM = [[ProvinceModel alloc]parseFromDic:cityDic];
                if (![provinceM.m_provinceID isEqualToString:@"567"]) {
                    [dataArray addObject:provinceM];
                }
            }
        }
        sucBlock_(dataArray);
    } andFailedBlock:^(ErrorModel *errorM) {
        failedBlock_(errorM);
    }];
}
//            //写入磁盘
//            NSString *filename = [CommonDeal writeData:dic andNet:isNet];
//            NSLog(@"%@",filename);
@end
