//
//  WujieApply.h
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "BaseApply.h"

@interface WujieApply : BaseApply

/*! 释放取WujieApply对象
 *\param    param:          无
 *\returns  returns:        返回WujieApply对象
 */
- (void)dismissWujieApply;

/*!单利模式获取WujieApply对象
 *\param    param:          无
 *\returns  returns:        返回WujieApply对象
 */
+ (WujieApply *)shareInstanceManager;

/*!发送版本号
 *\param    param:              版本号
 *\returns  sucBlock_:          返回成功的数据
 *\returns  failedBlock_:       返回失败的数据
 */
- (void)sendVerson:(NSString *)param andSuccessBlock:(SuccessBlock)sucBlock_ andFailedBlock:(FailedBlock)failedBlock_;

/*!获取城市
 *\param    isNet:              0全部；1仅有网点的城市
 *\returns  sucBlock_:          返回成功的数据
 *\returns  failedBlock_:       返回失败的数据
 */
-(void)getCity:(BOOL)isNet andSuccessBlock:(SuccessBlock)sucBlock_ andFailedBlock:(FailedBlock)failedBlock_;
@end
