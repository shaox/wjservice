//
//  BaseApply.h
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ErrorModel.h"
#import "HttpWjService.h"
#import "ProvinceModel.h"
#import "CityModel.h"

typedef void(^ReturnBlock)(BOOL flag);                            //方法的回调正确或者错误
typedef void(^ReturnBlockContent)(id content);                    //方法的回调的数据
typedef void(^FaildReturnBlock) (ErrorModel *errorM);                       //错误信息回调

@interface BaseApply : NSObject

@end
