//
//  AFNetworkManager.h
//  LR
//
//  Created by 邵 旭 on 14-6-4.
//  Copyright (c) 2014年 tyrbl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPClient.h"

@interface AFNetworkManager : AFHTTPClient

/*!单例模式，获取请求管理类
*\param        param:   无
*\returns      return:     无
*/
+ (AFNetworkManager *)shareManagement;

/*!\注册为JSON请求
 *\param        param:   无
 *\returns      return:     无
 */
- (void)registerAFJSONRequestOperation;

/*!\注册为XML请求
 *\param        param:   无
 *\returns      return:     无
 */
- (void)registerAFXMLRequestOperation;

/*!\鉴权
 *\param        param:   无
 *\returns      return:     无
 */
- (void)authentication;

@end
