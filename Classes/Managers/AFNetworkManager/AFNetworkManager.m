//
//  AFNetworkManager.m
//  LR
//
//  Created by 邵 旭 on 14-6-4.
//  Copyright (c) 2014年 tyrbl. All rights reserved.
//

#import "AFNetworkManager.h"
#import "SettingManager.h"
#import "AFNetworking.h"

static AFNetworkManager *m_AFNetworkManagement;

@implementation AFNetworkManager

#pragma mark -
#pragma mark public methods

+ (AFNetworkManager *)shareManagement
{
    static dispatch_once_t onceTocken;
    dispatch_once(&onceTocken, ^{
//        NSString *test = nil;
//        test = @"jsonBasicUrlTestXXD";
//        NSString *urlString = [SettingManage getBasicUrlWithKey:test];
//        NSURL *basicURL = [NSURL URLWithString:urlString];
//        m_AFNetworkManagement = [[AFNetworkManager alloc] initWithBaseURL:basicURL];
        m_AFNetworkManagement = [[AFNetworkManager alloc] initWithBaseURL:[NSURL URLWithString:kNetworkNoti]];
    });
    return m_AFNetworkManagement;
}

- (void)registerAFJSONRequestOperation
{
    [self registerHTTPOperationClass:[AFHTTPRequestOperation class]];
}

//- (void)registerAFXMLRequestOperation
//{
//    [self registerHTTPOperationClass:[AFXMLRequestOperation class]];
//}
//
//- (void)authentication
//{
//    [self clearAuthorizationHeader];
//    NSString *authName = [SettingManage getAuthMessageWithKey:@"authName"];
//    NSString *autpPass = [SettingManage getAuthMessageWithKey:@"authPass"];
//    [self setAuthorizationHeaderWithUsername:authName
//                                    password:autpPass];
//}

#pragma mark -
#pragma mark private methods

//重写父类方法，重置请求超时
- (NSMutableURLRequest *)requestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters
{
    NSMutableURLRequest *request = [super requestWithMethod:method path:path parameters:parameters];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setTimeoutInterval:60];
    return request;
}

- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (!self)
    {
        return nil;
    }
    [self setDefaultHeader:@"Accept" value:@"application/json"];
    [self setParameterEncoding:AFFormURLParameterEncoding];
    if ([[url scheme] isEqualToString:@"https"] &&
        [[url host] isEqualToString:@"alpha-api.app.net"])
    {
        self.defaultSSLPinningMode = AFSSLPinningModePublicKey;
    }else{
        self.defaultSSLPinningMode = AFSSLPinningModeNone;
    }
    return self;
}

@end
