//
//  FileManager.h
//  LR
//
//  Created by Tianweiwei on 14-5-14.
//  Copyright (c) 2014年 tyrbl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject

/**单类设计
 *\param        param:      无
 *\returns      return:     无
 */
+ (id)shareManager;

/**创建发送消息图片的到本地
 *\param        image:      图片
 *\param        imageUrl:      图片上传成功的服务端的路径
 *\returns      return:     图片保存的路径
 */
- (NSString *)createMsgImage:(UIImage *)image andImageUrl:(NSString *)imageUrl andFileName:(NSString *)filename;

@end
