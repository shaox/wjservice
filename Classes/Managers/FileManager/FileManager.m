//
//  FileManager.m
//  LR
//
//  Created by Tianweiwei on 14-5-14.
//  Copyright (c) 2014年 tyrbl. All rights reserved.
//

#import "FileManager.h"
#import "Common.h"

@interface FileManager()
{
    NSFileManager *m_fileManager;
}

@property(nonatomic,retain)NSFileManager *m_fileManager;

/**检查当前目录的路径是否存在
 *\param        param:      无
 *\returns      return:     无
 */
- (BOOL)checkPathIsExist:(NSString *)path;

/**创建文件的目录
 *\param        param:      需要创建的路径
 *\returns      return:     无
 */
- (void)addNewFolder:(NSString *)path;

/**图片到本地文件中
 *\param        image:      图片
 *\param        path:      图片的路径
 *\param        path:      图片的名称
 *\returns      return:     无
 */
- (NSString *)localPhotos:(UIImage *)image path:(NSString *)path andImagePath:(NSString *)imgUrl;
@end

static FileManager *m_manager;
@implementation FileManager
@synthesize m_fileManager;


+ (id)shareManager
{
    if (!m_manager)
    {
        m_manager = [[FileManager alloc] init];
    }
    return m_manager;
}

- (id)init
{
    self = [super init];
    if(self)
    {
      self.m_fileManager = [NSFileManager defaultManager];
    }
   return self;
}

- (BOOL)checkPathIsExist:(NSString *)path
{
    return [m_fileManager fileExistsAtPath:path isDirectory:nil];
}

- (NSString *)createMsgImage:(UIImage *)image andImageUrl:(NSString *)imageUrl andFileName:(NSString *)filename
{
    if (![self checkPathIsExist:filename])
    {
        [self addNewFolder:filename];
    }
    else{
        [self removeLocalPhoto:filename];
    }
    return [self localPhotos:image path:filename andImagePath:imageUrl];
}

//新建目录,path为目录路径(包含目录名)
- (void)addNewFolder:(NSString *)path
{
    [m_fileManager createDirectoryAtPath:path
             withIntermediateDirectories:YES
                              attributes:nil
                                   error:nil];
}

//上传图片
- (NSString *)localPhotos:(UIImage *)image path:(NSString *)path andImagePath:(NSString *)imgUrl
{
    
    NSData *imgData = UIImagePNGRepresentation(image);
    imgUrl = [CommonDeal JAVAImageNameToIOSImageName:imgUrl andFlag:YES];
    path = [path stringByAppendingPathComponent:imgUrl];
    if ([m_fileManager createFileAtPath:path contents:imgData attributes:nil])
    {
        return path;
    }else{
        return nil;
    }
}

-(void)removeLocalPhoto:(NSString *)path
{
    if([m_fileManager fileExistsAtPath:path])
    {
        NSArray *nameArray = [m_fileManager subpathsAtPath:path];
        for (int i=0; i<([nameArray count]); i++) {
            [m_fileManager removeItemAtPath:[NSString stringWithFormat:@"%@/%@",path,[nameArray objectAtIndex:i]] error:nil];
        }
    }
}
@end
