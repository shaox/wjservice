//
//  FirstViewController.m
//  WJService
//
//  Created by 邵 旭 on 15/3/18.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "FirstViewController.h"
#import "FirstView.h"
#import "TabbarViewController.h"
#import "AppDelegate.h"
@interface FirstViewController ()<FirstDelegate>
{
    FirstView *m_firstView;
}
@property (nonatomic,strong) FirstView *p_firstView;
@end

@implementation FirstViewController
@synthesize p_firstView = m_firstView;

-(void)dealloc
{
    
}

-(void)loadView
{
    [super loadView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_firstView.p_firstDelegate = self;
    [m_firstView refreshFirstView];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [m_firstView scrollView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - FirstDelegate

-(void)gotoLogin:(id)sender
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TabbarViewController *tabbar = [storyboard instantiateViewControllerWithIdentifier:@"HomeID"];
    [delegate.window setRootViewController:tabbar];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
