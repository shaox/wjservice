//
//  DemandViewController.m
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "DemandViewController.h"
#import "DemandView.h"
#import "DemandDetailViewController.h"

@interface DemandViewController ()<DemandDelegate>
{
    DemandView *m_demandView;
    UIBarButtonItem *m_leftBarItem;
    UILabel *m_titleLbl;
    BOOL m_isFirst;
    NSString *m_newUrl;
}
@property(nonatomic,strong) IBOutlet DemandView *p_demandView;
@property (nonatomic,strong) UIBarButtonItem *p_leftBarItem;
@property (nonatomic,strong) UILabel *p_titleLbl;
@end

@implementation DemandViewController
@synthesize p_demandView = m_demandView;
@synthesize p_leftBarItem = m_leftBarItem;
@synthesize p_titleLbl = m_titleLbl;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (IsIOS7) {
        self.navigationController.navigationBar.barTintColor = otherBarColor;
    }
    m_demandView.p_demandDelegate = self;
    [m_demandView loadServiceDemand:kDemandURL];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDemandType:) name:@"refreshDemand" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadWeb:) name:@"isLogin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadWeb:) name:@"isLogout" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(network:) name:@"Network" object:nil];
    self.p_leftBarItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ico-back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"写需求";
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)goBack:(id)sender
{
    if (m_demandView.p_demandWebView.canGoBack && !m_isFirst){
        [m_demandView.p_demandWebView goBack];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshDemandType:(NSNotification *)noti
{
    NSString *str = (NSString *)noti.object;
    m_newUrl = str;
    [m_demandView loadServiceDemand:str];
}

-(void)reloadWeb:(NSNotification *)noti
{
    [m_demandView.p_demandWebView reload];
}

#pragma mark - DemandDelegate

-(void)showDemandBar:(NSString *)detailUrl
{
    [self performSegueWithIdentifier:@"DemandDetailSegue" sender:detailUrl];
}

-(void)showDemandTab:(BOOL)isShow
{
    m_isFirst = isShow;
    if (isShow) {
        self.tabBarController.tabBar.hidden = NO;
        self.navigationItem.leftBarButtonItem = nil;
    }else{
        self.tabBarController.tabBar.hidden = YES;
        self.navigationItem.leftBarButtonItem = m_leftBarItem;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"DemandDetailSegue"])
    {
        NSString *str = (NSString *)sender;
        DemandDetailViewController *detail = segue.destinationViewController;
        detail.p_detailUrl = str;
    }
}


@end
