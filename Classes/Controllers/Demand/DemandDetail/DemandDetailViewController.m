//
//  DemandDetailViewController.m
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "DemandDetailViewController.h"
#import "DemandDetailView.h"
#import "MenuPicker.h"
#import "ProvinceModel.h"
#import "CityModel.h"
#import "AppContextManager.h"
@interface DemandDetailViewController ()<DemandDetailDelegate,MenuPickerDelegate>
{
    DemandDetailView *m_demandDetailView;
    MenuPicker *m_menuPicker;
    NSString *m_newStr;
}
@property(nonatomic,strong) IBOutlet DemandDetailView *p_demandDetailView;
@property(nonatomic,strong) MenuPicker *p_menuPicker;
@end

@implementation DemandDetailViewController
@synthesize p_demandDetailView = m_demandDetailView;
@synthesize p_menuPicker = m_menuPicker;
@synthesize p_detailUrl = m_detailUrl;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [m_menuPicker remove];
    self.p_menuPicker = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_demandDetailView.p_demandDetailDelegate = self;
    [m_demandDetailView loadServiceDemandDetail:m_detailUrl];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(network:) name:@"Network" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadWebnet:) name:@"Service" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (m_newStr) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshDemand" object:m_newStr];
    }
    [m_menuPicker remove];
}

-(IBAction)goBack:(id)sender
{
    if (m_demandDetailView.p_demandWebView.canGoBack){
        [m_demandDetailView.p_demandWebView goBack];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)reloadWebnet:(NSNotification *)noti
{
    [m_demandDetailView.p_demandWebView reload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DemandDetailDelegate

-(void)webviewTitleFormDemand:(NSString *)titleStr
{
    [m_menuPicker remove];
    NSArray *arr = [titleStr componentsSeparatedByString:@"_"];
    if (arr.count>0) {
        self.navigationItem.title = [arr firstObject];
    }
}

-(void)showDemandFirst:(NSString *)url andMethod:(NSInteger)index
{
    m_newStr = url;
    
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)selectDemandAddress:(NSString *)cityStr
{
    [m_menuPicker remove];
    self.p_menuPicker = [[MenuPicker alloc] initPickviewWithArray:AppManage.p_allPCArray isHaveNavControler:NO];
    m_menuPicker.p_menuPickerDelegate=self;
    [self.view addSubview:m_menuPicker];
    dispatch_queue_t addQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(addQueue, ^{
        NSInteger comp = 0;
        NSInteger row = 0;
        NSArray *arr = [cityStr componentsSeparatedByString:@";"];
        for (int i=0; i<AppManage.p_allPCArray.count; i++) {
            ProvinceModel *provinceM = [AppManage.p_allPCArray objectAtIndex:i];
            if ([provinceM.m_provinceID isEqualToString:[arr firstObject]]) {
                comp = i;
                for (int k=0; k<provinceM.m_cityArray.count; k++) {
                    CityModel *cityM = [provinceM.m_cityArray objectAtIndex:k];
                    if ([cityM.m_cityID isEqualToString:[arr lastObject]]) {
                        row = k;
                    }
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [m_menuPicker reloadPicker:comp andRow:row];
        });
    });
}

-(void)demandDetailSelect:(id)sender
{
    [m_menuPicker remove];
    self.p_menuPicker = [[MenuPicker alloc] initPickviewWithArray:AppManage.p_allPCArray isHaveNavControler:NO];
    m_menuPicker.p_menuPickerDelegate=self;
    [self.view addSubview:m_menuPicker];
    dispatch_queue_t addQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(addQueue, ^{
        NSInteger comp = 0;
        NSInteger row = 0;
        dispatch_async(dispatch_get_main_queue(), ^{
            [m_menuPicker reloadPicker:comp andRow:row];
        });
    });
}

#pragma mark MenuPickerDelegate

-(void)toobarDonBtnHaveClick:(MenuPicker *)pickView resultString:(NSString *)resultString
{
    [m_demandDetailView sendDemandResultString:resultString];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
