//
//  TabbarViewController.m
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "TabbarViewController.h"
#import "HomeNavViewController.h"
#import "WujieApply.h"
#import "AppContextManager.h"
@interface TabbarViewController ()
{
    BOOL m_isLoading;
}
@property(nonatomic,assign) BOOL p_isLoading;
@end

@implementation TabbarViewController
@synthesize p_isLoading = m_isLoading;

-(void)dealloc
{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (IsIOS7) {
        self.tabBar.tintColor = [UIColor colorWithRed:236/255.0f green:115/255.0f blue:77/255.0f alpha:1.0f];
    }
    else{
        CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
        CGContextFillRect(context, rect);
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.tabBar.backgroundImage = image;
    }

    self.selectedIndex = 2;
    UITabBarItem *item1=[self.tabBar.items objectAtIndex:2];
    
    UIImage *item1Image  = [UIImage imageNamed:@"tab-item2.png"];
    UIImage *item1ImageSelected =[UIImage imageNamed:@"tab-item2-select.png"];
    if (IsIOS7) {
        item1Image = [item1Image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        item1ImageSelected = [item1ImageSelected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [item1 setImage:item1Image];
        [item1 setSelectedImage:item1ImageSelected];
    }else{
        UITabBarItem *tabBarItems = [[UITabBarItem alloc]initWithTitle:@"" image:item1Image tag:2];
        [tabBarItems setSelectedImage:item1ImageSelected];
        tabBarItems.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
        HomeNavViewController *homeNav = [self.viewControllers objectAtIndex:2];
        homeNav.tabBarItem = tabBarItems;
    }
//    [self sendVerson:YES];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(netWork:) name:@"Network" object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)netWork:(NSNotification *)noti
{
//    [self sendVerson:m_isLoading];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
