//
//  ServiceNetworkViewController.m
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "ServiceNetworkViewController.h"
#import "ServiceNetworkView.h"
#import "ServiceNetDetialViewController.h"
#import "MenuPicker.h"
#import "ProvinceModel.h"
#import "CityModel.h"
#import "AppContextManager.h"
@interface ServiceNetworkViewController ()<ServiceNetworkDelegate,MenuPickerDelegate,UIActionSheetDelegate>
{
    ServiceNetworkView *m_serviceNetworkView;
    MenuPicker *m_menuPicker;
    NSString *m_numberStr;
}
@property(nonatomic,strong) IBOutlet ServiceNetworkView *p_serviceNetworkView;
@property(nonatomic,strong) MenuPicker *p_menuPicker;
@end

@implementation ServiceNetworkViewController
@synthesize p_serviceNetworkView = m_serviceNetworkView;
@synthesize p_menuPicker = m_menuPicker;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [m_menuPicker remove];
    self.p_menuPicker = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    m_serviceNetworkView.p_serviceNetworkDelegate = self;
    [m_serviceNetworkView loadServiceNetworkWeb:kServiceURL];
    if (IsIOS7) {
        self.navigationController.navigationBar.barTintColor = otherBarColor;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadWebnet:) name:@"isLogin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadWebnet:) name:@"isLogout" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadWebnet:) name:@"Service" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(netBack:) name:@"refreshNetFirst" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"网点服务";
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [m_menuPicker remove];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)reloadWebnet:(NSNotification *)noti
{
    [m_serviceNetworkView.p_serviceNetworkWebView reload];
}

-(void)netBack:(NSNotification *)noti
{
    [m_serviceNetworkView loadServiceNetworkWeb:(NSString *)noti.object];
}

#pragma mark MenuPickerDelegate

-(void)toobarDonBtnHaveClick:(MenuPicker *)pickView resultString:(NSString *)resultString
{
    [m_serviceNetworkView sendNetResultString:resultString];
}

#pragma mark - ServiceNetworkDelegate
-(void)getServiceNetworkStatus:(id)sender
{
    if (![CommonDeal checkNetwork]) {
        [[YBProgressShow defaultProgress] showText:@"无网络"
                                            InMode:MBProgressHUDModeText
                             OriginViewEnableTouch:NO
                                HideAfterDelayTime:1.5f];
    }
}

-(void)showNetworkDetail:(NSString *)networkUrl
{
    [self performSegueWithIdentifier:@"ServiceDetailSegue" sender:networkUrl];
}


-(void)selectNetAddress:(NSString *)selectStr
{
    [m_menuPicker remove];
    self.p_menuPicker = [[MenuPicker alloc] initPickviewWithArray:AppManage.p_servicePCArray isHaveNavControler:YES];
    m_menuPicker.p_menuPickerDelegate=self;
    [self.view addSubview:m_menuPicker];
    dispatch_queue_t addQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(addQueue, ^{
        NSInteger comp = 0;
        NSInteger row = 0;
        NSArray *arr = [selectStr componentsSeparatedByString:@";"];
        for (int i=0; i<AppManage.p_servicePCArray.count; i++) {
            ProvinceModel *provinceM = [AppManage.p_servicePCArray objectAtIndex:i];
            if ([provinceM.m_provinceID isEqualToString:[arr firstObject]]) {
                comp = i;
                for (int k=0; k<provinceM.m_cityArray.count; k++) {
                    CityModel *cityM = [provinceM.m_cityArray objectAtIndex:k];
                    if ([cityM.m_cityID isEqualToString:[arr lastObject]]) {
                        comp = i;
                        row = k;
                    }
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [m_menuPicker reloadPicker:comp andRow:row];
        });
    });
}

-(void)networkSelect:(id)sender
{
    [m_menuPicker remove];
    self.p_menuPicker = [[MenuPicker alloc] initPickviewWithArray:AppManage.p_servicePCArray isHaveNavControler:YES];
    m_menuPicker.p_menuPickerDelegate=self;
    [self.view addSubview:m_menuPicker];
    dispatch_queue_t addQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(addQueue, ^{
        NSInteger comp = 0;
        NSInteger row = 0;
        dispatch_async(dispatch_get_main_queue(), ^{
            [m_menuPicker reloadPicker:comp andRow:row];
        });
    });
}

#pragma mark - setHead

-(void)setHeadActionSheet:(NSString *)number
{
    [m_menuPicker remove];
    m_numberStr = number;
    UIActionSheet* mySheet = [[UIActionSheet alloc]
                              initWithTitle:nil
                              delegate:self
                              cancelButtonTitle:@"取消"
                              destructiveButtonTitle:nil
                              otherButtonTitles:@"电话",@"短信",nil];
    mySheet.actionSheetStyle = UIActionSheetStyleAutomatic;
    mySheet.tag = 10;
    [mySheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)modalView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            if ([UIApplication instancesRespondToSelector:@selector(canOpenURL:)])  {
                NSString *telNumber = [NSString stringWithFormat:@"tel:%@",m_numberStr];
                NSURL *aURL = [NSURL URLWithString: telNumber];
                if ([[UIApplication sharedApplication] canOpenURL:aURL]){
                    [[UIApplication sharedApplication] openURL:aURL];
                }
                m_numberStr = nil;
            }
            break;
        }
        case 1:
        {
            if ([UIApplication instancesRespondToSelector:@selector(canOpenURL:)])  {
                NSString *telNumber = [NSString stringWithFormat:@"sms://%@",m_numberStr];
                NSURL *aURL = [NSURL URLWithString: telNumber];
                if ([[UIApplication sharedApplication] canOpenURL:aURL]){
                    [[UIApplication sharedApplication] openURL:aURL];
                }
                m_numberStr = nil;
            }
            break;
        }
        default:
            break;
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ServiceDetailSegue"])
    {
        NSString *str = (NSString *)sender;
        ServiceNetDetialViewController *detail = segue.destinationViewController;
        detail.p_networkDetailUrl = str;
    }
}


@end
