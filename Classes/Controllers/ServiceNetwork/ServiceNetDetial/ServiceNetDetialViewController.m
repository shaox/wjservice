//
//  ServiceNetDetialViewController.m
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "ServiceNetDetialViewController.h"
#import "ServiceNetDetailView.h"
@interface ServiceNetDetialViewController ()<NetworkDetailDelegate>
{
    ServiceNetDetailView *m_serviceNetDetailView;
    NSString *m_url;
}
@property(nonatomic,strong) IBOutlet ServiceNetDetailView *p_serviceNetDetailView;
@end

@implementation ServiceNetDetialViewController
@synthesize p_serviceNetDetailView = m_serviceNetDetailView;
@synthesize p_networkDetailUrl = m_networkDetailUrl;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_serviceNetDetailView.p_networkDetailDelegate = self;
    [m_serviceNetDetailView loadServiceNetworkDetail:m_networkDetailUrl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (m_url) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshNetFirst" object:m_url];
    }
}

-(IBAction)goBack:(id)sender
{
    if (m_serviceNetDetailView.p_netWebView.canGoBack){
        [m_serviceNetDetailView.p_netWebView goBack];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - NetworkDetailDelegate
-(void)webviewTitleFormNetwork:(NSString *)titleStr
{
    NSArray *arr = [titleStr componentsSeparatedByString:@"_"];
    if (arr.count>0) {
        self.navigationItem.title = [arr firstObject];
    }
}

-(void)gotoFirstNet:(NSString *)url
{
    m_url = url;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
