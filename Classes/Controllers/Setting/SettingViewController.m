//
//  SettingViewController.m
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "SettingViewController.h"
#import "SettingView.h"
#import "SetDetailViewController.h"
#import "AppContextManager.h"

@interface SettingViewController ()<SettingDelegate>
{
    SettingView *m_settingView;
    UIBarButtonItem *m_leftBarItem;
    UILabel *m_titleLbl;
}
@property (nonatomic,strong) IBOutlet SettingView *p_settingView;
@property (nonatomic,strong) UIBarButtonItem *p_leftBarItem;
@property (nonatomic,strong) UILabel *p_titleLbl;
@end

@implementation SettingViewController
@synthesize p_settingView = m_settingView;
@synthesize p_leftBarItem = m_leftBarItem;
@synthesize p_titleLbl = m_titleLbl;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_settingView.p_settingDelegate = self;
    [m_settingView loadSettingWeb:kSetingURL];
    if (IsIOS7) {
        self.navigationController.navigationBar.barTintColor = otherBarColor;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSetView:) name:@"isLogin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSetPass:) name:@"refreshSetPass" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"设置";
    [m_settingView.p_settingWebView reload];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (!AppManage.p_isLogin) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"isLogout" object:nil];
    }
}

-(void)goBack:(id)sender
{
    if (m_settingView.p_settingWebView.canGoBack){
        [m_settingView.p_settingWebView goBack];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshSetView:(NSNotification *)noti
{
    [m_settingView.p_settingWebView reload];
}

-(void)refreshSetPass:(NSNotification *)noti
{
    [m_settingView loadSettingWeb:(NSString *)noti.object];
}

#pragma mark - SettingDelegate
-(void)showSetDetail:(NSString *)url
{
    [self performSegueWithIdentifier:@"SetDetailSegue" sender:url];
}

-(void)logoutApp:(NSString *)url
{
    AppManage.p_isLogin = NO;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SetDetailSegue"])
    {
        NSString *str = (NSString *)sender;
        SetDetailViewController *detail = segue.destinationViewController;
        detail.p_setDetailUrl = str;
    }
}


@end
