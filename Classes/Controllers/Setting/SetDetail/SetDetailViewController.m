//
//  SetDetailViewController.m
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "SetDetailViewController.h"
#import "SetDetailView.h"
@interface SetDetailViewController ()<SetDetailDelegate>
{
    SetDetailView *m_setDetaiView;
    NSString *m_url;
}
@property(nonatomic,strong) IBOutlet SetDetailView *p_setDetaiView;
@end

@implementation SetDetailViewController
@synthesize p_setDetaiView = m_setDetaiView;
@synthesize p_setDetailUrl = m_setDetailUrl;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_setDetaiView.p_setDetailDelegate = self;
    [m_setDetaiView loadServiceSetDetail:m_setDetailUrl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (m_url) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshSetPass" object:m_url];
    }
}

-(IBAction)goBack:(id)sender
{
    if (m_setDetaiView.p_setWebView.canGoBack){
        [m_setDetaiView.p_setWebView goBack];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - SetDetailDelegate
-(void)webviewTitleFormSet:(NSString *)titleStr
{
    NSArray *arr = [titleStr componentsSeparatedByString:@"_"];
    if (arr.count>0) {
        self.navigationItem.title = [arr firstObject];
    }
}

-(void)gotoFirst:(NSString *)url
{
    m_url = url;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
