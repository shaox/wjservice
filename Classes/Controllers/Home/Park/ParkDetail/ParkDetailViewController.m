//
//  ParkDetailViewController.m
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "ParkDetailViewController.h"
#import "ParkDetailView.h"
@interface ParkDetailViewController ()<ParkDetailDelegate,UIGestureRecognizerDelegate>
{
    ParkDetailView *m_parkDetailView;
    NSString *m_parkUrl;
    NSString *m_secondUrl;
}
@property(nonatomic,strong) IBOutlet ParkDetailView *p_parkDetailView;
@end

@implementation ParkDetailViewController
@synthesize p_parkDetailUrl = m_parkDetailUrl;
@synthesize p_parkDetailView = m_parkDetailView;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_parkDetailView.p_parkDetailDelegate = self;
    [m_parkDetailView loadServiceParkDetail:m_parkDetailUrl];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(network:) name:@"Network" object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (m_parkUrl) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"checkPark" object:m_parkUrl];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goBack:(id)sender
{
    NSRange secondview = [m_secondUrl rangeOfString:@"view/id"];
    
    if (secondview.location != NSNotFound) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        if (m_parkDetailView.p_parkDetailWebView.canGoBack){
            [m_parkDetailView.p_parkDetailWebView goBack];
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - ParkDetailDelegate

-(void)imageHidden:(BOOL)isHidden
{
    if (isHidden) {
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }else{
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

-(void)webviewTitleFormPark:(NSString *)titleStr
{
    NSArray *arr = [titleStr componentsSeparatedByString:@"_"];
    if (arr.count>0) {
        self.navigationItem.title = [arr firstObject];
    }
}

-(void)showDemandFirst:(NSString *)url
{
    m_parkUrl = url;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveDetailImage:(NSString *)url
{
    UIImage *image = [CommonDeal getImageFromURL:url];
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), nil);
}

- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString *msg = nil ;
    if(error != NULL){
        msg = @"保存图片失败" ;
    }else{
        msg = @"保存图片成功" ;
    }
    [[YBProgressShow defaultProgress] showText:msg
                                        InMode:MBProgressHUDModeText
                         OriginViewEnableTouch:NO
                            HideAfterDelayTime:1];
}

-(void)navColor:(NSString *)url
{
    m_secondUrl = url;
//    NSRange login = [url rangeOfString:@"login"];
//    NSRange registers = [url rangeOfString:@"register"];
//    if (login.location != NSNotFound || registers.location != NSNotFound){
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
//        self.navigationController.navigationBar.barTintColor = loginBarColor;
//        self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
//        self.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];
//    }
//    else{
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//        self.navigationController.navigationBar.barTintColor = otherBarColor;
//        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
//        self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
//    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
