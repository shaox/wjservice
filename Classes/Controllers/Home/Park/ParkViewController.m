//
//  ParkViewController.m
//  WJService
//
//  Created by 邵 旭 on 15-3-5.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "ParkViewController.h"
#import "ParkView.h"
#import "SINavigationMenuView.h"
#import "ParkDetailViewController.h"

@interface ParkViewController ()<SINavigationMenuDelegate,ParkDelegate>
{
    ParkView *m_parkView;
    SINavigationMenuView *m_menu;
    NSMutableArray *m_titleArray;
    UILabel *m_textLbl;
    BOOL reload;
}
@property(nonatomic,strong) IBOutlet ParkView *p_parkView;
@property(nonatomic,strong) SINavigationMenuView *p_menu;
@property(nonatomic,strong) NSMutableArray *p_titleArray;
@property(nonatomic,strong) UILabel *p_textLbl;
@end

@implementation ParkViewController
@synthesize p_parkView = m_parkView;
@synthesize p_menu = m_menu;
@synthesize p_titleArray = m_titleArray;
@synthesize p_textLbl = m_textLbl;
@synthesize p_actionStr = m_actionStr;

-(void)dealloc
{
    self.p_menu = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)loadView
{
    [super loadView];
    self.p_titleArray = [NSMutableArray arrayWithObjects:@"园区", @"技术", @"人才", @"资本", @"培训-专题", @"培训-专家", @"培训-机构",nil];
    NSString *titleStr = [m_titleArray objectAtIndex:m_actionStr.integerValue];
    if (self.navigationItem) {
        CGRect frame = CGRectMake(0.0, 0.0, 100.0, self.navigationController.navigationBar.bounds.size.height);
        self.p_menu = [[SINavigationMenuView alloc] initWithFrame:frame title:titleStr andSelect:m_actionStr];
        [m_menu displayMenuInView:self.navigationController.view];
        m_menu.delegate = self;
        m_menu.items = [NSArray arrayWithArray:m_titleArray];
        [self performSelector:@selector(showNavButton) withObject:self afterDelay:1.0f];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_parkView.p_parkDelegate = self;
    [self loadURL:m_actionStr.integerValue];
    self.p_textLbl = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0, 200.0, self.navigationController.navigationBar.bounds.size.height)];
    m_textLbl.backgroundColor = [UIColor clearColor];
    m_textLbl.textColor = [UIColor whiteColor];
    m_textLbl.textAlignment = NSTextAlignmentCenter;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPark:) name:@"checkPark" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(network:) name:@"Network" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (reload && [CommonDeal checkNetwork]) {
        [m_parkView.p_parkWebView reload];
        reload = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [m_menu onHideMenu];
}

-(void)showNavButton
{
    self.navigationItem.titleView = m_menu;
}

#pragma mark - NSNotification

-(void)refreshPark:(NSNotification *)noti
{
    reload = NO;
    NSString *str = (NSString *)noti.object;
    [m_parkView loadParkWeb:str];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backToHome:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - SINavigationMenuDelegate
- (void)didSelectItemAtIndex:(NSUInteger)index
{
    NSString *title = [m_titleArray objectAtIndex:index];
    [m_menu changeTitle:title];
    [self loadURL:index];
}

-(void)loadURL:(NSUInteger)index
{
    m_actionStr = [NSString stringWithFormat:@"%ld",(unsigned long)index];
    switch (index) {
        case 0:
        {
            [m_parkView loadParkWeb:Park];
            break;
        }
        case 1:
        {
            [m_parkView loadParkWeb:Technology];
            break;
        }
        case 2:
        {
            [m_parkView loadParkWeb:Talent];
            break;
        }
        case 3:
        {
            [m_parkView loadParkWeb:Capital];
            break;
        }
        case 4:
        {
            [m_parkView loadParkWeb:TrainClass];
            break;
        }
        case 5:
        {
            [m_parkView loadParkWeb:TrainExperts];
            break;
        }
        case 6:
        {
            [m_parkView loadParkWeb:Mechanism];
            break;
        }
        default:
            break;
    }
}

#pragma mark - ParkDelegate
-(void)showParkDetail:(NSString *)parkUrl
{
    reload = YES;
    [self performSegueWithIdentifier:@"ParkDetailSegue" sender:parkUrl];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ParkDetailSegue"])
    {
        NSString *str = (NSString *)sender;
        ParkDetailViewController *detail = segue.destinationViewController;
        detail.p_parkDetailUrl = str;
    }
}

@end
