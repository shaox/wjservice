//
//  HomeTableViewController.m
//  WJService
//
//  Created by 邵 旭 on 15/3/13.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "HomeTableViewController.h"
#import "HomeCell.h"
#import "ParkViewController.h"
#import "SearchViewController.h"
#import "HomeSearchBar.h"
#import "MenuView.h"
#import "SearchResultCell.h"
#import "WujieApply.h"


@interface HomeTableViewController ()<HomeCellDelegate,UISearchDisplayDelegate,UISearchBarDelegate,UISearchControllerDelegate,HomeSearchDelegate,MenuDelegate,SearchResultDelegate,UIGestureRecognizerDelegate>
{
    HomeSearchBar *m_searchBar;
    MenuView *m_menuView;
    NSMutableArray *m_cellArray;
    UIBarButtonItem *m_leftBarItem;
    UIImageView *m_titleviewImage;
    UILabel *m_titleviewLbl;
}
@property(nonatomic,strong) IBOutlet HomeSearchBar *p_searchBar;
@property(nonatomic,strong) MenuView *p_menuView;
@property(nonatomic,strong) NSMutableArray *p_cellArray;
@property(nonatomic,strong) UIBarButtonItem *p_leftBarItem;
@property(nonatomic,strong) UIImageView *p_titleviewImage;
@property(nonatomic,strong) UILabel *p_titleviewLbl;
@end

@implementation HomeTableViewController
@synthesize p_searchBar = m_searchBar;
@synthesize p_menuView = m_menuView;
@synthesize p_cellArray = m_cellArray;
@synthesize p_leftBarItem = m_leftBarItem;
@synthesize p_titleviewImage = m_titleviewImage;
@synthesize p_titleviewLbl = m_titleviewLbl;

-(void)dealloc
{
    
}

-(void)loadView
{
    [super loadView];
    
    m_searchBar.layer.borderColor = [[UIColor groupTableViewBackgroundColor]CGColor];
    m_searchBar.layer.borderWidth = 1;
    m_searchBar.p_homeSearchDelegate = self;
    if ([CommonDeal checkDevice] && IsIOS7) {
        m_searchBar.returnKeyType = UIReturnKeySearch;
    }
    
    if (!IsIOS7) {
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:234/255.0f green:115/255.0f blue:79/255.0f alpha:1.0f];
    }else{
        self.navigationController.navigationBar.barTintColor = otherBarColor;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.p_titleviewImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg-homeBar.png"]];
    m_titleviewImage.contentMode = UIViewContentModeScaleAspectFit;
    self.p_titleviewLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 21)];
    m_titleviewLbl.textAlignment = NSTextAlignmentCenter;
    m_titleviewLbl.textColor = [UIColor whiteColor];
    m_titleviewLbl.backgroundColor = [UIColor clearColor];
    self.p_leftBarItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ico-back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
    m_leftBarItem.tintColor = [UIColor whiteColor];
    m_leftBarItem.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    
    self.navigationItem.titleView = m_titleviewImage;
    [self.tableView setContentOffset:CGPointMake(0.0, 44.0) animated:NO];
    self.searchDisplayController.searchResultsTableView.scrollEnabled = NO;
    self.p_cellArray = [[NSMutableArray alloc]init];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.searchDisplayController.searchResultsTableView]){
        return kMainScreen_Height-64;
    }
    return kHomeCellHeightFor4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:self.searchDisplayController.searchResultsTableView]){
        static NSString *CellIdentifierResultsTableView = @"ResultCell";
        SearchResultCell *cellResults = (SearchResultCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierResultsTableView];
        if (!cellResults)
        {
            cellResults = [[SearchResultCell alloc ] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierResultsTableView];
        }
        cellResults.selectionStyle = UITableViewCellSelectionStyleNone;
        cellResults.p_searchResultDelegate = self;
        [cellResults loadView];
        [m_cellArray addObject:cellResults];
        return cellResults;
    }
    else{
        static NSString *Home = @"HomeCell";
        HomeCell *cell = (HomeCell *)[tableView dequeueReusableCellWithIdentifier:Home forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[HomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Home];
        }
        cell.p_homeCellDelegate = self;
        [cell createHomeCell];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    m_menuView.hidden = YES;
}

- (void) searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    m_menuView.hidden = YES;
}

#pragma mark - HomeCellDelegate
-(void)clickWithHomeAction:(HomeAction)action
{
    switch (action) {
        case clickPark:
            [self performSegueWithIdentifier:@"GotoParkSegue" sender:@"0"];
            break;
        case clickSkill:
            [self performSegueWithIdentifier:@"GotoParkSegue" sender:@"1"];
            break;
        case clickTalent:
            [self performSegueWithIdentifier:@"GotoParkSegue" sender:@"2"];
            break;
        case clickCapital:
            [self performSegueWithIdentifier:@"GotoParkSegue" sender:@"3"];
            break;
        case clickClass:
            [self performSegueWithIdentifier:@"GotoParkSegue" sender:@"4"];
            break;
        case clickExperts:
            [self performSegueWithIdentifier:@"GotoParkSegue" sender:@"5"];
            break;
        case clickMechanism:
            [self performSegueWithIdentifier:@"GotoParkSegue" sender:@"6"];
            break;
        default:
            break;
    }
}

#pragma mark - UISearchBar delegate

- (void) searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    if (controller.isActive) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
}

- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    if (controller.isActive) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    m_menuView.hidden = YES;
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    m_menuView.hidden = YES;
    if ([m_searchBar.text isEqualToString:@" "]) {
        [[YBProgressShow defaultProgress] showText:@"请输入内容"
                                            InMode:MBProgressHUDModeText
                             OriginViewEnableTouch:NO
                                HideAfterDelayTime:1.5f];
        return;
    }
    
    NSString *commandStr = kSearchURL([self changeTextToEnglish:m_searchBar.p_menuBtn.titleLabel.text],m_searchBar.text);
    commandStr = [commandStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (m_cellArray.count>0) {
        [searchBar resignFirstResponder];
        SearchResultCell *cellResults = [m_cellArray lastObject];
        [cellResults loadServiceSearch:commandStr];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    m_menuView.hidden = YES;
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}

-(NSString *)changeTextToEnglish:(NSString *)type
{
    NSString *str = nil;
    if ([type isEqualToString:@"园区"]) {
        str = @"park";
    }
    else if ([type isEqualToString:@"技术"]) {
        str = @"technology";
    }
    else if ([type isEqualToString:@"人才"]) {
        str = @"person";
    }
    else if ([type isEqualToString:@"资本"]) {
        str = @"capital";
    }
    else if ([type isEqualToString:@"专题"]) {
        str = @"traincourse";
    }
    else if ([type isEqualToString:@"机构"]) {
        str = @"traininstitution";
    }
    else if ([type isEqualToString:@"专家"]) {
        str = @"trainexpert";
    }
    return str;
}

#pragma mark - SearchResultDelegate
-(void)resultWithUrl:(NSString *)url
{
    m_menuView.hidden = YES;
    [self performSegueWithIdentifier:@"SearchSegue" sender:url];
    
//    NSRange strRange = [url rangeOfString:@"/view/id"];
//    if (strRange.location != NSNotFound){
//        self.navigationItem.titleView = m_titleviewLbl;
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
//        self.navigationItem.leftBarButtonItem = m_leftBarItem;
//        self.tabBarController.tabBar.hidden = YES;
//    }
}

-(void)goBack:(id)sender
{
//    SearchResultCell *cellResults = [m_cellArray lastObject];
//    if (cellResults.p_cellWebView.canGoBack){
//        [cellResults.p_cellWebView goBack];
//    }
//    else{
//
//    }
//    m_titleviewLbl.text = nil;
//    self.navigationItem.titleView = m_titleviewImage;
//    self.navigationItem.leftBarButtonItem = nil;
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
//    self.tabBarController.tabBar.hidden = NO;
}

-(void)resultWithTitle:(NSString *)title
{
    NSArray *arr = [title componentsSeparatedByString:@"_"];
    if (arr.count>0 && [self.navigationItem.titleView isKindOfClass:[UILabel class]]) {
        m_titleviewLbl.text = [arr firstObject];;
    }
}

#pragma mark - HomeSearchDelegate
-(void)menuClick:(id)sender
{
//    UIButton *btn = (UIButton *)sender;
    [m_searchBar resignFirstResponder];
    
    if (!self.p_menuView) {
        self.p_menuView = [[MenuView alloc]initWithFrame:CGRectMake(3, 54, 194, 284)];
        m_menuView.backgroundColor = [UIColor clearColor];
        m_menuView.p_menuDelegate = self;
//        m_menuView.layer.borderColor = [[UIColor redColor]CGColor];
//        m_menuView.layer.borderWidth = 1;
//        [self.view addSubview:m_menuView];
//        [self.view.superview addSubview:m_menuView];
        [self.view.window addSubview:m_menuView];
    }
    else{
        m_menuView.hidden = NO;
    }
}

#pragma mark - MenuDelegate
-(void)selectType:(NSString *)title
{
    NSArray *arr = [title componentsSeparatedByString:@"-"];
    [m_searchBar.p_menuBtn setTitle:[arr lastObject] forState:UIControlStateNormal];
    m_menuView.hidden = YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"GotoParkSegue"])
    {
        NSString *action = (NSString *)sender;
        ParkViewController *park = segue.destinationViewController;
        park.p_actionStr = action;
    }
    if ([segue.identifier isEqualToString:@"SearchSegue"])
    {
        NSString *action = (NSString *)sender;
        SearchViewController *search = segue.destinationViewController;
        search.p_searchUrl = action;
    }
}


@end
