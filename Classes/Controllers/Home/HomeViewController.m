//
//  HomeViewController.m
//  WJService
//
//  Created by 邵 旭 on 15-3-6.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeView.h"
#import "WujieApply.h"
@interface HomeViewController ()<HomeDelegate>
{
    HomeView *m_homeView;
}
@property(nonatomic,strong) IBOutlet HomeView *p_homeView;
@end

@implementation HomeViewController
@synthesize p_homeView = m_homeView;

-(void)dealloc
{
    
}

-(void)loadView
{
    [super loadView];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_homeView.p_homeDelegate = self;
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg-homeBar.png"]];
    self.navigationItem.titleView = image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)queryHomeData
{
    [WjApply queryHomeData:^(id backStr) {
        
    } andFailedBlock:^(ErrorModel *errorM) {
        NSLog(@"errorM:%@",errorM.m_message);
    }];
}

#pragma mark - HomeDelegate
-(void)gotoPark:(id)sender
{
    [self performSegueWithIdentifier:@"GotoParkSegue" sender:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
