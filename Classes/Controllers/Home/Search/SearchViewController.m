//
//  SearchViewController.m
//  WJService
//
//  Created by 邵 旭 on 15/3/20.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchView.h"
@interface SearchViewController ()<SearchViewDelegate>
{
    SearchView *m_searchView;
    NSString *m_secondUrl;
}
@property(nonatomic,strong) IBOutlet SearchView *p_searchView;
@end

@implementation SearchViewController
@synthesize p_searchView = m_searchView;
@synthesize p_searchUrl = m_searchUrl;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_searchView.p_searchViewDelegate = self;
    [m_searchView loadServiceResult:m_searchUrl];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(network:) name:@"Network" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goBack:(id)sender
{
    NSRange secondview = [m_secondUrl rangeOfString:@"view/id"];
    
    if (secondview.location != NSNotFound) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        if (m_searchView.p_webView.canGoBack){
            [m_searchView.p_webView goBack];
        }
        else{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma mark - SearchViewDelegate
-(void)titles:(NSString *)url
{
    m_secondUrl = url;
}


-(void)imageSearchHidden:(BOOL)isHidden
{
    if (isHidden) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }else{
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

-(void)webviewTitleFormSearch:(NSString *)titleStr
{
    NSArray *arr = [titleStr componentsSeparatedByString:@"_"];
    if (arr.count>0) {
        self.navigationItem.title = [arr firstObject];
    }
}

-(void)saveSearchImage:(NSString *)url
{
    UIImage *image = [CommonDeal getImageFromURL:url];
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(imageSavedToPhotosAlbums:didFinishSavingWithError:contextInfo:), nil);
}

- (void)imageSavedToPhotosAlbums:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString *msg = nil ;
    if(error != NULL){
        msg = @"保存图片失败" ;
    }else{
        msg = @"保存图片成功" ;
    }
    [[YBProgressShow defaultProgress] showText:msg
                                        InMode:MBProgressHUDModeText
                         OriginViewEnableTouch:NO
                            HideAfterDelayTime:1];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
