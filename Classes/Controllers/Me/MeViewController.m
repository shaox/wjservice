//
//  MeViewController.m
//  WJService
//
//  Created by 邵 旭 on 15/3/17.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "MeViewController.h"
#import "MeView.h"
#import "MeDetailViewController.h"
#import "AppContextManager.h"

@interface MeViewController ()<MeDelegate>
{
    MeView *m_meView;
    UIBarButtonItem *m_leftBarItem;
    BOOL m_isFirstLoad;
}
@property (nonatomic,strong) IBOutlet MeView *p_meView;
@property (nonatomic,strong) UIBarButtonItem *p_leftBarItem;
@property (nonatomic,assign) BOOL p_isFirstLoad;
@end

@implementation MeViewController
@synthesize p_meView = m_meView;
@synthesize p_leftBarItem = m_leftBarItem;
@synthesize p_isFirstLoad = m_isFirstLoad;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_meView.p_meDelegate = self;
    self.p_isFirstLoad = NO;
    [m_meView loadMeWeb:kMeURL];
    if (IsIOS7) {
        self.navigationController.navigationBar.barTintColor = otherBarColor;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMeWeb:) name:@"refreshLoginStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadLogout:) name:@"isLogout" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadLogout:) name:@"refresh" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.navigationItem.title = @"用户";
}

-(void)goBack:(id)sender
{
    if (m_meView.p_meWebView.canGoBack){
        [m_meView.p_meWebView goBack];
    }
}

-(void)reloadMeWeb:(NSNotification *)noti
{
    AppManage.p_isLogin = YES;
    [m_meView loadMeWeb:(NSString *)noti.object];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"isLogin" object:nil];
    
}

-(void)reloadLogout:(NSNotification *)noti
{
    [m_meView.p_meWebView reload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - MeDelegate
-(void)webviewTitleFormMeFirst:(NSString *)titleStr
{
    NSArray *arr = [titleStr componentsSeparatedByString:@"_"];
    if (arr.count>0) {
        self.navigationItem.title = [arr firstObject];
    }
}

-(void)showMeDetail:(NSString *)detailUrl
{
    [self performSegueWithIdentifier:@"MeDetailSegue" sender:detailUrl];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"MeDetailSegue"])
    {
        NSString *str = (NSString *)sender;
        MeDetailViewController *detail = segue.destinationViewController;
        detail.p_meDetailUrl = str;
    }
}

@end
