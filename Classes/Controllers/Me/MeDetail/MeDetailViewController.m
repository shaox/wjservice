//
//  MeDetailViewController.m
//  WJService
//
//  Created by 邵 旭 on 15/3/30.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "MeDetailViewController.h"
#import "MeDetailView.h"
#import "MenuPicker.h"
#import "ProvinceModel.h"
#import "CityModel.h"
#import "AppContextManager.h"
@interface MeDetailViewController ()<MeDetailDelegate,MenuPickerDelegate>
{
    MeDetailView *m_meDetailView;
    MenuPicker *m_menuPicker;
    NSString *m_url;
    NSString *m_gobackUrl;
}
@property (nonatomic,strong) IBOutlet MeDetailView *p_meDetailView;
@property(nonatomic,strong) MenuPicker *p_menuPicker;
@end

@implementation MeDetailViewController
@synthesize p_meDetailView = m_meDetailView;
@synthesize p_meDetailUrl = m_meDetailUrl;
@synthesize p_menuPicker = m_menuPicker;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [m_menuPicker remove];
    self.p_menuPicker = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    m_meDetailView.p_meDetailDelegate = self;
    [m_meDetailView loadServiceMeDetail:m_meDetailUrl];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadWebnet:) name:@"Service" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSRange login = [m_meDetailUrl rangeOfString:@"login"];
    NSRange registers = [m_meDetailUrl rangeOfString:@"register"];
    if (login.location != NSNotFound || registers.location != NSNotFound){
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        self.navigationController.navigationBar.barTintColor = loginBarColor;
        self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
        self.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (m_url) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshLoginStatus" object:m_url];
    }
    if (m_gobackUrl) {
        m_gobackUrl = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
    }
    
    NSRange login = [m_meDetailUrl rangeOfString:@"login"];
    NSRange registers = [m_meDetailUrl rangeOfString:@"register"];
    if (login.location != NSNotFound || registers.location != NSNotFound){
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        self.navigationController.navigationBar.barTintColor = otherBarColor;
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goBack:(id)sender
{
    if (m_gobackUrl){
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        if (m_meDetailView.p_meWebView.canGoBack){
            [m_meDetailView.p_meWebView goBack];
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void)reloadWebnet:(NSNotification *)noti
{
    [m_meDetailView.p_meWebView reload];
}

#pragma mark MenuPickerDelegate

-(void)toobarDonBtnHaveClick:(MenuPicker *)pickView resultString:(NSString *)resultString
{
    [m_meDetailView sendResultString:resultString];
}

#pragma mark - NetworkDetailDelegate

-(void)selectAddress:(NSString *)cityStr
{
    [m_menuPicker remove];
    self.p_menuPicker = [[MenuPicker alloc] initPickviewWithArray:AppManage.p_allPCArray isHaveNavControler:NO];
    m_menuPicker.p_menuPickerDelegate=self;
    [self.view addSubview:m_menuPicker];
    dispatch_queue_t addQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(addQueue, ^{
        NSInteger comp = 0;
        NSInteger row = 0;
        NSArray *arr = [cityStr componentsSeparatedByString:@";"];
        for (int i=0; i<AppManage.p_allPCArray.count; i++) {
            ProvinceModel *provinceM = [AppManage.p_allPCArray objectAtIndex:i];
            if ([provinceM.m_provinceID isEqualToString:[arr firstObject]]) {
                comp = i;
                for (int k=0; k<provinceM.m_cityArray.count; k++) {
                    CityModel *cityM = [provinceM.m_cityArray objectAtIndex:k];
                    if ([cityM.m_cityID isEqualToString:[arr lastObject]]) {

                        row = k;
                    }
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [m_menuPicker reloadPicker:comp andRow:row];
        });
    });
}

-(void)meDetaillSelect:(id)sender
{
    [m_menuPicker remove];
    self.p_menuPicker = [[MenuPicker alloc] initPickviewWithArray:AppManage.p_allPCArray isHaveNavControler:NO];
    m_menuPicker.p_menuPickerDelegate=self;
    [self.view addSubview:m_menuPicker];
    dispatch_queue_t addQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(addQueue, ^{
        NSInteger comp = 0;
        NSInteger row = 0;
        dispatch_async(dispatch_get_main_queue(), ^{
            [m_menuPicker reloadPicker:comp andRow:row];
        });
    });
}

-(void)webviewTitleFormMe:(NSString *)titleStr
{
    [m_menuPicker remove];
    NSArray *arr = [titleStr componentsSeparatedByString:@"_"];
    if (arr.count>0) {
        self.navigationItem.title = [arr firstObject];
    }
}

-(void)showMeFirst:(NSString *)url
{
    [self.navigationController popToRootViewControllerAnimated:NO];
    m_url = url;
}

-(void)saveMeDetailImage:(NSString *)url
{
    UIImage *image = [CommonDeal getImageFromURL:url];
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), nil);
}

- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString *msg = nil ;
    if(error != NULL){
        msg = @"保存图片失败" ;
    }else{
        msg = @"保存图片成功" ;
    }
    [[YBProgressShow defaultProgress] showText:msg
                                        InMode:MBProgressHUDModeText
                         OriginViewEnableTouch:NO
                            HideAfterDelayTime:1];
}

-(void)imageHiddenMeDetail:(BOOL)isHidden
{
    if (isHidden) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }else{
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

-(void)backRefresh:(NSString *)url
{
    m_gobackUrl = url;
}

-(void)gobackNormal:(id)sender
{
    m_gobackUrl = nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
