//
//  ErrorModel.h
//  CJVideo
//
//  Created by 邵 旭 on 14-12-3.
//  Copyright (c) 2014年 tyrbl. All rights reserved.
//

#import "BasicModel.h"

@interface ErrorModel : BasicModel
{
    int m_state;                //错误状态码
    NSString *m_type;           //错误类型
    NSString *m_message;        //错误原因
    NSString *m_stacks;         //错误接口
}

@property (nonatomic,assign) int m_state;
@property (nonatomic,strong) NSString *m_message;
@property (nonatomic,strong) NSString *m_type;
@property (nonatomic,strong) NSString *m_stacks;

@end
