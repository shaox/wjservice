//
//  ProvinceModel.m
//  WJService
//
//  Created by 邵 旭 on 15/3/26.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "ProvinceModel.h"

#define kProvinceID @"id"
#define kProvinceName @"name"
#define kCity   @"cityList"
#define kSelect @"selected"

@implementation ProvinceModel
@synthesize m_provinceID;
@synthesize m_provinceName;
@synthesize m_cityArray;
@synthesize m_isSelect;

- (id)init
{
    self = [super init];
    if(self)
    {
        m_provinceID = @"";
        m_provinceName = @"";
        m_isSelect = NO;
    }
    return self;
}

- (id)parseFromDic:(NSDictionary *)dic
{
    if([self init])
    {
        if([CommonDeal jugementStringIsNil:[dic objectForKey:kProvinceID]])
        {
            self.m_provinceID = @"";
        }else{
            self.m_provinceID = [NSString stringWithFormat:@"%@",[dic objectForKey:kProvinceID]];
        }
        
        if([CommonDeal jugementStringIsNil:[dic objectForKey:kProvinceName]])
        {
            self.m_provinceName = @"";
        }else{
            self.m_provinceName = [NSString stringWithFormat:@"%@",[dic objectForKey:kProvinceName]];
        }
        
        if([CommonDeal jugementStringIsNil:[dic objectForKey:kSelect]])
        {
            self.m_isSelect = NO;
        }else{
            if ([[dic objectForKey:kSelect] isEqualToString:@"1"]) {
                self.m_isSelect = YES;
            }
            else{
                self.m_isSelect = NO;
            }
        }
        
        self.m_cityArray = [[NSMutableArray alloc]init];
        if ([[dic objectForKey:kCity] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *cityDic = [dic objectForKey:kCity];
            
            for (int i=0; i<cityDic.allKeys.count; i++) {
                CityModel *cityM = [[CityModel alloc]parseFromDic:[cityDic objectForKey:[cityDic.allKeys objectAtIndex:i]]];
                [m_cityArray addObject:cityM];
            }
        }
        else if ([[dic objectForKey:kCity] isKindOfClass:[NSArray class]]) {
            NSArray *cityArr = [dic objectForKey:kCity];
            
            for (int i=0; i<cityArr.count; i++) {
                CityModel *cityM = [[CityModel alloc]parseFromDic:[cityArr objectAtIndex:i]];
                [m_cityArray addObject:cityM];
            }
        }
    }
    return self;
}

@end
