//
//  ProvinceModel.h
//  WJService
//
//  Created by 邵 旭 on 15/3/26.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "BasicModel.h"
#import "CityModel.h"
@interface ProvinceModel : BasicModel
{
    NSString *m_provinceID;     //省份id
    NSString *m_provinceName;   //省份名称
    BOOL m_isSelect;            //是否选中
    NSMutableArray *m_cityArray;    //城市
}
@property(nonatomic,strong) NSString *m_provinceID;
@property(nonatomic,strong) NSString *m_provinceName;
@property(nonatomic,strong) NSMutableArray *m_cityArray;
@property(nonatomic,assign) BOOL m_isSelect;

- (id)parseFromDic:(NSDictionary *)dic;

@end
