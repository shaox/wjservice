//
//  UserModel.m
//  Alumi
//
//  Created by 邵 旭 on 15-1-7.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "UserModel.h"

#define kUserID         @"studentid"    //用户id
#define kUserAge        @"age"          //用户年龄
#define kUserBirthday   @"birthday"     //用户生日
#define kUserEmail      @"email"        //用户邮箱
#define kUserHeadUrl    @"headImg"      //用户头像url
#define kUserNickName   @"nickName"     //用户昵称
#define kUserPhone      @"phone"        //用户电话
#define kUserSexy       @"sex"          //用户性别
#define kUserSignature  @"signature"    //用户个性签名

#define kSchool         @"tschool"      //学校

#define kUserIsLogin    @"islogin"      //用户是否登录
#define kUserIsOnline   @"isonline"     //用户是否在线

#define kKey            @"student"      //字典key

@implementation UserModel
@synthesize m_username;
@synthesize m_password;

@synthesize m_personID;
@synthesize m_personName;
@synthesize m_nickName;
@synthesize m_personHeadUrl;
@synthesize m_personPhoneNumber;
@synthesize m_personEmail;
@synthesize m_sexy;
@synthesize m_signature;
@synthesize m_birthday;
@synthesize m_personAge;

- (id)init
{
    self = [super init];
    if(self)
    {
        m_username = @"";
        m_password = @"";
        m_personID = @"";
        m_personName = @"";
        m_personHeadUrl = @"";
        m_personPhoneNumber = @"";
        m_personEmail = @"";
        m_sexy = YES;
        m_signature = @"";
    }
    return self;
}

- (id)parseFromDic:(NSDictionary *)dic
{
    if([self init])
    {
        if([CommonDeal jugementStringIsNil:[dic valueForKeyPath:kUserID]])
        {
            self.m_personID = @"";
        }else{
            self.m_personID = [NSString stringWithFormat:@"%@",[dic valueForKeyPath:kUserID]];
        }
        
        if([CommonDeal jugementStringIsNil:[dic valueForKeyPath:kUserAge]])
        {
            self.m_personAge = @"";
        }else{
            self.m_personAge = [NSString stringWithFormat:@"%@",[dic valueForKeyPath:kUserAge]];
        }
        
        if([CommonDeal jugementStringIsNil:[dic valueForKeyPath:kUserBirthday]])
        {
            self.m_birthday = @"";
        }else{
            self.m_birthday = [NSString stringWithFormat:@"%@",[dic valueForKeyPath:kUserBirthday]];
        }
        
        if([CommonDeal jugementStringIsNil:[dic valueForKeyPath:kUserEmail]])
        {
            self.m_personEmail = @"";
        }else{
            self.m_personEmail = [NSString stringWithFormat:@"%@",[dic valueForKeyPath:kUserEmail]];
        }

        if([CommonDeal jugementStringIsNil:[dic valueForKeyPath:kUserHeadUrl]])
        {
            self.m_personHeadUrl = @"";
        }else{
            self.m_personHeadUrl = [NSString stringWithFormat:@"http://c19899281.jsp.fjjsp01.com/%@",[[dic valueForKeyPath:kUserHeadUrl] stringByReplacingOccurrencesOfString:@"_" withString:@"/"]];
        }
        
        if([CommonDeal jugementStringIsNil:[dic valueForKeyPath:kUserNickName]])
        {
            self.m_nickName = @"";
        }else{
            self.m_nickName = [NSString stringWithFormat:@"%@",[dic valueForKeyPath:kUserNickName]];
        }
        
        if([CommonDeal jugementStringIsNil:[dic valueForKeyPath:kUserPhone]])
        {
            self.m_personPhoneNumber = @"";
        }else{
            self.m_personPhoneNumber = [NSString stringWithFormat:@"%@",[dic valueForKeyPath:kUserPhone]];
        }
        
        if([CommonDeal jugementStringIsNil:[dic valueForKeyPath:kUserSexy]])
        {
            self.m_sexy = NO;
        }else{
            if ([[NSString stringWithFormat:@"%@",[dic valueForKeyPath:kUserSexy]] isEqualToString:@"0"]) {
                self.m_sexy = NO;
            }
            else{
                self.m_sexy = YES;
            }
        }
        
        if([CommonDeal jugementStringIsNil:[dic valueForKeyPath:kUserSignature]])
        {
            self.m_signature = @"";
        }else{
            self.m_signature = [NSString stringWithFormat:@"%@",[dic valueForKeyPath:kUserSignature]];
        }
    
    }
    return self;
}
@end
