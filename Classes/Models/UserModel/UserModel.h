//
//  UserModel.h
//  Alumi
//
//  Created by 邵 旭 on 15-1-7.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "BasicModel.h"
@interface UserModel : BasicModel
{
    //对应数据库student表
    NSString *m_username;               //账号
    NSString *m_password;               //密码
    
    NSString *m_personID;               //用户id
    NSString *m_personName;             //用户姓名
    NSString *m_nickName;               //昵称
    NSString *m_personHeadUrl;          //用户头像url
    NSString *m_personPhoneNumber;      //用户手机号码
    NSString *m_personEmail;            //用户邮箱
    BOOL m_sexy;                        //性别,YES为男性
    NSString *m_signature;              //个性签名
    NSString *m_birthday;               //生日
    NSString *m_personAge;              //年龄
    
}
@property (nonatomic,strong) NSString *m_username;
@property (nonatomic,strong) NSString *m_password;

@property (nonatomic,strong) NSString *m_personID;
@property (nonatomic,strong) NSString *m_personName;
@property (nonatomic,strong) NSString *m_nickName;
@property (nonatomic,strong) NSString *m_personHeadUrl;
@property (nonatomic,strong) NSString *m_personPhoneNumber;
@property (nonatomic,strong) NSString *m_personEmail;
@property (nonatomic,assign) BOOL m_sexy;
@property (nonatomic,strong) NSString *m_signature;
@property (nonatomic,strong) NSString *m_birthday;
@property (nonatomic,strong) NSString *m_personAge;

- (id)parseFromDic:(NSDictionary *)dic;
@end
