//
//  CityModel.m
//  WJService
//
//  Created by 邵 旭 on 15/3/26.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "CityModel.h"

#define kCityID @"id"
#define kCityName @"name"
#define kCirtySelect @"selected"

@implementation CityModel
@synthesize m_cityID;
@synthesize m_cityName;
@synthesize m_citySelect;

- (id)init
{
    self = [super init];
    if(self)
    {
        m_cityID = @"";
        m_cityName = @"";
        m_citySelect = NO;
    }
    return self;
}

- (id)parseFromDic:(NSDictionary *)dic
{
    if([self init])
    {
        if([CommonDeal jugementStringIsNil:[dic objectForKey:kCityID]])
        {
            self.m_cityID = @"";
        }else{
            self.m_cityID = [NSString stringWithFormat:@"%@",[dic objectForKey:kCityID]];
        }
        
        if([CommonDeal jugementStringIsNil:[dic objectForKey:kCityName]])
        {
            self.m_cityName = @"";
        }else{
            self.m_cityName = [NSString stringWithFormat:@"%@",[dic objectForKey:kCityName]];
        }
        
        if([CommonDeal jugementStringIsNil:[dic objectForKey:kCirtySelect]])
        {
            self.m_citySelect = NO;
        }else{
            if ([[dic objectForKey:kCirtySelect] isEqualToString:@"1"]) {
                self.m_citySelect = YES;
            }
            else{
                self.m_citySelect = NO;
            }
        }
    }
    return self;
}


@end
