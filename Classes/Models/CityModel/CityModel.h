//
//  CityModel.h
//  WJService
//
//  Created by 邵 旭 on 15/3/26.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "BasicModel.h"

@interface CityModel : BasicModel
{
    NSString *m_cityID;     //城市id
    NSString *m_cityName;   //城市名称
    BOOL m_citySelect;
}
@property(nonatomic,strong) NSString *m_cityID;
@property(nonatomic,strong) NSString *m_cityName;
@property(nonatomic,assign) BOOL m_citySelect;

- (id)parseFromDic:(NSDictionary *)dic;
@end
