//
//  BasicModel.h
//  CJVideo
//
//  Created by 邵 旭 on 14-12-3.
//  Copyright (c) 2014年 tyrbl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BasicModel : NSObject

/*!解析字典中的数据
 *\param            dic:需要解析的字典
 *\returns          无
 **/
- (id)parseFromDic:(NSDictionary *)dic;


@end
