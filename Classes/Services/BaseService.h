//
//  BaseService.h
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ErrorModel.h"
#import "AFXMLRequestOperation.h"
#import "AFNetworkManager.h"
#import "AFJSONRequestOperation.h"

typedef void(^SuccessBlock)(id backStr);
typedef void(^FailedBlock)(ErrorModel *errorM);

@interface BaseService : NSObject
{
    SuccessBlock success_block;
    FailedBlock failed_block;
}

@end
