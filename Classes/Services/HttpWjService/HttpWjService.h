//
//  HttpWjService.h
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "BaseService.h"

@interface HttpWjService : BaseService<NSXMLParserDelegate>

/*!通过http上传版本
 \*param            param:版本号
 \*returns          无
 **/
-(void)queryVersonInfo:(NSString *)param andSuccessBlock:(SuccessBlock)sucBlock_ andFailedBlock:(FailedBlock)failedBlock_;

/*!通过http获取城市信息
 \*param            isNet:0全部；1仅有网点的城市
 \*returns          无
 **/
-(void)queryCityData:(BOOL)isNet andSuccessBlock:(SuccessBlock)sucBlock_ andFailedBlock:(FailedBlock)failedBlock_;
@end
