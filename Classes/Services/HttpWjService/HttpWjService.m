//
//  HttpWjService.m
//  WJService
//
//  Created by 邵 旭 on 15-3-11.
//  Copyright (c) 2015年 tyrbl. All rights reserved.
//

#import "HttpWjService.h"

@implementation HttpWjService

-(void)queryVersonInfo:(NSString *)param andSuccessBlock:(SuccessBlock)sucBlock_ andFailedBlock:(FailedBlock)failedBlock_
{
    if(![CommonDeal checkNetwork])
    {
        ErrorModel *errorM = [[ErrorModel alloc] init];
        errorM.m_message = @"连接失败";
        errorM.m_state = -1;
        failedBlock_(errorM);
        return;
    }

    [AFNetworkManage registerAFJSONRequestOperation];
    NSString *commandStr = kVersonURL;
    NSLog(@"commandStr:%@",commandStr);
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [CommonDeal setSafetyObject:[NSString stringWithFormat:@"I;%@",param] forKey:@"info" andDic:dic];
    NSMutableURLRequest *request = [AFNetworkManage requestWithMethod:@"POST" path:commandStr parameters:dic];
    
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/html",@"text/plain", nil]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        NSString *code = [NSString stringWithFormat:@"%@",[JSON valueForKeyPath:@"code"]];
        if ([code isEqualToString:@"1"]) {
            sucBlock_(JSON);
        }
        else{
            ErrorModel *errorM = [[ErrorModel alloc] init];
            errorM.m_state = [code intValue];
            if ([JSON valueForKeyPath:@"message"]) {
                errorM.m_message = [JSON valueForKeyPath:@"message"];
            }else{
                errorM.m_message = @"";
            }
            errorM.m_stacks = [NSString stringWithFormat:@"%@",JSON];
            failedBlock_(errorM);
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        ErrorModel *errorM = [[ErrorModel alloc] init];
        errorM.m_state = (int)error.code;
        errorM.m_type = @"";
        errorM.m_stacks = [[error userInfo] objectForKey:@"NSLocalizedDescription"];
        failedBlock_(errorM);
    }];
    [operation start];
}

-(void)queryCityData:(BOOL)isNet andSuccessBlock:(SuccessBlock)sucBlock_ andFailedBlock:(FailedBlock)failedBlock_
{
    if(![CommonDeal checkNetwork])
    {
        ErrorModel *errorM = [[ErrorModel alloc] init];
        errorM.m_message = @"连接失败";
        errorM.m_state = -1;
        failedBlock_(errorM);
        return;
    }
    [AFNetworkManage registerAFJSONRequestOperation];
    NSString *commandStr = kCityURL;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    NSLog(@"commandStr:%@",commandStr);
    [CommonDeal setSafetyObject:[NSString stringWithFormat:@"%d",isNet] forKey:@"type" andDic:dic];
    NSMutableURLRequest *request = [AFNetworkManage requestWithMethod:@"POST" path:commandStr parameters:dic];
    
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/html",@"text/plain", nil]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
     {
         NSString *code = [NSString stringWithFormat:@"%@",[JSON valueForKeyPath:@"code"]];
         if ([code isEqualToString:@"1"]) {
             sucBlock_(JSON);
         }
         else{
             ErrorModel *errorM = [[ErrorModel alloc] init];
             errorM.m_state = [code intValue];
             if ([JSON valueForKeyPath:@"message"]) {
                 errorM.m_message = [JSON valueForKeyPath:@"message"];
             }else{
                 errorM.m_message = @"";
             }
             errorM.m_stacks = [NSString stringWithFormat:@"%@",JSON];
             failedBlock_(errorM);
         }
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
         ErrorModel *errorM = [[ErrorModel alloc] init];
         errorM.m_state = (int)error.code;
         errorM.m_type = @"";
         errorM.m_stacks = [[error userInfo] objectForKey:@"NSLocalizedDescription"];
         failedBlock_(errorM);
     }];
    [operation start];

}

@end
