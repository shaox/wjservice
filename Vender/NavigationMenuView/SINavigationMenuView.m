//
//  SINavigationMenuView.m
//  NavigationMenu
//
//  Created by Ivan Sapozhnik on 2/19/13.
//  Copyright (c) 2013 Ivan Sapozhnik. All rights reserved.
//

#import "SINavigationMenuView.h"
#import "SIMenuButton.h"
#import "QuartzCore/QuartzCore.h"
#import "SIMenuConfiguration.h"

@interface SINavigationMenuView  ()<UIGestureRecognizerDelegate>
{
    UITapGestureRecognizer *tap;
}
@property (nonatomic, strong) SIMenuButton *menuButton;
@property (nonatomic, strong) SIMenuTable *table;
@property (nonatomic, strong) UIView *menuContainer;
@property (nonatomic, strong) UIImageView *menuBg;
@property (nonatomic, strong) UIView *menuBackgroud;
@property (nonatomic,strong)  NSString *selectStr;
@end

@implementation SINavigationMenuView

- (id)initWithFrame:(CGRect)frame title:(NSString *)title andSelect:(NSString *)str
{
    self = [super initWithFrame:frame];
    if (self) {
        frame.origin.y += 1.0;
        self.selectStr = str;
        self.menuButton = [[SIMenuButton alloc] initWithFrame:frame];
        self.menuButton.title.text = title;
        self.menuButton.title.textAlignment = NSTextAlignmentCenter;
        self.menuButton.title.textColor = [UIColor whiteColor];
        [self.menuButton addTarget:self action:@selector(onHandleMenuTap:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.menuButton];

    }
    return self;
}

- (void)displayMenuInView:(UIView *)view
{
    self.menuContainer = view;
    self.menuBackgroud = [[UIView alloc]initWithFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y+self.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height, view.frame.size.width, view.frame.size.height-(self.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height))];
    self.menuBackgroud.hidden = YES;
    self.menuBackgroud.backgroundColor = [UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:0.2f];
    [self.menuContainer addSubview:self.menuBackgroud];
}

-(void)changeTitle:(NSString *)title
{
    self.menuButton.title.text = title;
}

-(void)hideTap:(id)sender
{
    [self.menuBackgroud removeGestureRecognizer:tap];
    self.menuButton.isActive = !self.menuButton.isActive;
    [self onHandleMenuTap:nil];
}

#pragma mark -
#pragma mark Actions
- (void)onHandleMenuTap:(id)sender
{
    if (self.menuButton.isActive) {
        [self onShowMenu];
    } else {
        [self onHideMenu];
    }
}

- (void)onShowMenu
{
    if (!self.table) {
        UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
        CGRect frame = mainWindow.frame;
        frame.origin.y += self.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
        self.menuBg = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"bg-pop.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10]];
        self.menuBg.frame = CGRectMake((kMainScreen_Width-156)/2, frame.origin.y-8.5f, 156, 260);
        [self.menuContainer addSubview:self.menuBg];
        self.table = [[SIMenuTable alloc] initWithFrame:CGRectMake((kMainScreen_Width-156)/2, frame.origin.y+3, 156, 250) items:self.items andSelect:self.selectStr.intValue];
        self.table.menuDelegate = self;
        tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideTap:)];
    }
    
    [self.menuBackgroud addGestureRecognizer:tap];
    self.menuBackgroud.hidden = NO;
    self.menuBg.hidden = NO;
    [self.menuContainer addSubview:self.table];
    
    [self rotateArrow:M_PI];//箭头旋转
    [self.table show];
}

- (void)onHideMenu
{
    self.menuBackgroud.hidden = YES;
    self.menuBg.hidden = YES;
    [self rotateArrow:0];
    [self.table hide];
}

- (void)rotateArrow:(float)degrees
{
    [UIView animateWithDuration:[SIMenuConfiguration animationDuration] delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        self.menuButton.arrow.layer.transform = CATransform3DMakeRotation(degrees, 0, 0, 1);
    } completion:NULL];
}

#pragma mark -
#pragma mark Delegate methods
- (void)didSelectItemAtIndex:(NSUInteger)index
{
    self.menuButton.isActive = !self.menuButton.isActive;
    [self onHandleMenuTap:nil];
    [self.delegate didSelectItemAtIndex:index];
}

- (void)didBackgroundTap
{
    self.menuButton.isActive = !self.menuButton.isActive;
    [self onHandleMenuTap:nil];
}

#pragma mark -
#pragma mark Memory management
- (void)dealloc
{
    self.menuBackgroud = nil;
    self.items = nil;
    self.menuButton = nil;
    self.menuContainer = nil;
    self.menuBg = nil;
}

@end
