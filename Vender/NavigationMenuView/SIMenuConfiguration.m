//
//  SIMenuConfiguration.m
//  NavigationMenu
//
//  Created by Ivan Sapozhnik on 2/20/13.
//  Copyright (c) 2013 Ivan Sapozhnik. All rights reserved.
//

#import "SIMenuConfiguration.h"

@implementation SIMenuConfiguration
//Menu width
+ (float)menuWidth
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    return window.frame.size.width;
}

//Menu item height
+ (float)itemCellHeight
{
    return 35;
}

//Animation duration of menu appearence
+ (float)animationDuration
{
    return 0.3f;
}

//Menu substrate alpha value
+ (float)backgroundAlpha
{
    return 0.5;
}

//Menu alpha value
+ (float)menuAlpha
{
    return 0.8;
}

//Value of bounce
+ (float)bounceOffset
{
    return -7.0;
}

//Arrow image near title
+ (UIImage *)arrowImage
{
    return [UIImage imageNamed:@"arrow_down.png"];
}

//Distance between Title and arrow image
+ (float)arrowPadding
{
    return 13.0;
}

//Items color in menu
+ (UIColor *)itemsColor
{
    return [UIColor blackColor];
}

+ (UIColor *)mainColor
{
    return [UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f];
}

+ (float)selectionSpeed
{
    return 0.15;
}

+ (UIColor *)tableViewColor
{
    return [UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:0.6f];
}

+ (UIColor *)itemTextColor
{
    return [UIColor grayColor];
}

+ (UIColor *)itemTextSelectColor
{
    return [UIColor colorWithRed:235/255.0f green:115/255.0f blue:30/255.0f alpha:1.0f];
}

+ (UIColor *)selectionColor
{
    return [UIColor colorWithRed:113.0/255.0 green:114.0/255.0 blue:116.0/255.0 alpha:1.0];
}
@end
